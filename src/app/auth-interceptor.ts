import {Injectable} from '@angular/core';
import {HttpErrorResponse, HttpEvent, HttpInterceptor, HttpResponse} from '@angular/common/http';
import {Router} from '@angular/router';
import {catchError, tap} from 'rxjs/operators';
import {throwError} from 'rxjs';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {

  constructor(private router: Router) {

  }
  intercept(request, next) {
    return next.handle(request).pipe(
      catchError((err, caught) => {
        console.log(err);
        if (err instanceof HttpErrorResponse) {
          if (err.status === 401 && !request.url.includes('sesion')) {
            this.router.navigate(['login']);
          }
        }
        return throwError(err);
      })
    );
  }
}
