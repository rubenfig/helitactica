import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'login',
    pathMatch: 'full'
  },
  {
    path: 'home',
    loadChildren: './components/home/home.module#HomePageModule'
  },
  {
    path: 'list',
    loadChildren: './components/list/list.module#ListPageModule'
  },
  { path: 'login',
    loadChildren: './components/login/login.module#LoginPageModule'
  },
  { path: 'alumnos-listado',
    loadChildren: './components/alumnos-listado/alumnos-listado.module#AlumnosListadoPageModule'
  },
  { path: 'instructores-listado',
    loadChildren: './components/instructores-listado/instructores-listado.module#InstructoresListadoPageModule' },
  { path: 'reservas', loadChildren: './components/reservas/reservas.module#ReservasPageModule' },
  { path: 'resetear-password', loadChildren: './components/resetear-password/resetear-password.module#ResetearPasswordPageModule' },
  { path: 'cambiar-password', loadChildren: './components/cambiar-password/cambiar-password.module#CambiarPasswordPageModule' },
  { path: 'atencion', loadChildren: './components/atencion/atencion.module#AtencionPageModule' },
  { path: 'perfil', loadChildren: './components/perfil/perfil.module#PerfilPageModule' },
  { path: 'reservas-alumno', loadChildren: './components/reservas-alumno/reservas-alumno.module#ReservasAlumnoPageModule' },
  { path: 'reservas-detalle', loadChildren: './components/reservas-detalle/reservas-detalle.module#ReservasDetallePageModule' },
  { path: 'reservas-crear', loadChildren: './components/reservas-crear/reservas-crear.module#ReservasCrearPageModule' },
  { path: 'reservas-avion', loadChildren: './components/reservas-avion/reservas-avion.module#ReservasAvionPageModule' },
  { path: 'aprobar-reservas', loadChildren: './components/aprobar-reservas/aprobar-reservas.module#AprobarReservasPageModule' },
  { path: 'aviones-listado', loadChildren: './components/aviones-listado/aviones-listado.module#AvionesListadoPageModule' },
  { path: 'usuarios-listado', loadChildren: './components/usuarios-listado/usuarios-listado.module#UsuariosListadoPageModule' },
  { path: 'usuarios-crear', loadChildren: './components/usuarios-crear/usuarios-crear.module#UsuariosCrearPageModule' },
  { path: 'alumnos-editar', loadChildren: './components/alumnos-editar/alumnos-editar.module#AlumnosEditarPageModule' },
  { path: 'notificaciones-listado', loadChildren: './components/notificaciones-listado/notificaciones-listado.module#NotificacionesListadoPageModule' },
  { path: 'tipos-reportes-listado', loadChildren: './components/tipos-reportes-listado/tipos-reportes-listado.module#TiposReportesListadoPageModule' },
  { path: 'aeronaves-crear', loadChildren: './components/aeronaves-crear/aeronaves-crear.module#AeronavesCrearPageModule' },
  { path: 'aeronaves-mantenimiento', loadChildren: './components/aeronaves-mantenimiento/aeronaves-mantenimiento.module#AeronavesMantenimientoPageModule' },
  { path: 'crear-mantenimiento', loadChildren: './components/crear-mantenimiento/crear-mantenimiento.module#CrearMantenimientoPageModule' },
  { path: 'configuraciones', loadChildren: './components/configuraciones/configuraciones.module#ConfiguracionesPageModule' },
  { path: 'mapa-reserva', loadChildren: './components/mapa-reserva/mapa-reserva.module#MapaReservaPageModule' },
  { path: 'pistas-listado', loadChildren: './components/pistas-listado/pistas-listado.module#PistasListadoPageModule' },
  { path: 'agendar-cierre-pista', loadChildren: './components/agendar-cierre-pista/agendar-cierre-pista.module#AgendarCierrePistaPageModule' },
  { path: 'instructores-horario', loadChildren: './components/instructores-horario/instructores-horario.module#InstructoresHorarioPageModule' },
  { path: 'cargar-reporte', loadChildren: './components/cargar-reporte/cargar-reporte.module#CargarReportePageModule' },
  { path: 'ver-reporte', loadChildren: './components/ver-reporte/ver-reporte.module#VerReportePageModule' },
  { path: 'reportes', loadChildren: './components/reportes/reportes.module#ReportesPageModule' },
  { path: 'reportes-inicio', loadChildren: './components/reportes-inicio/reportes-inicio.module#ReportesInicioPageModule' },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
