export const reemplazarGuion = (obj) =>  {
  return typeof obj === 'string' ? obj.toString().replace(/-/g, "/") : obj;
};

export const menuPerfil = {
  reservas: {
    title: 'Reservas',
    url: '/reservas',
    icon: 'calendar'
  },
  reservas_instructor: {
    title: 'Reservas',
    url: '/reservas',
    icon: 'calendar'
  },
  alumnos: {
    title: 'Alumnos',
    url: '/alumnos-listado',
    icon: 'contacts'
  },
  aeronaves: {
    title: 'Aeronaves',
    url: '/aviones-listado',
    icon: 'airplane'
  },
  instructores: {
    title: 'Instructores',
    url: '/instructores-listado',
    icon: 'people'
  }
  ,
  perfil: {
    title: 'Perfil',
    url: '/perfil',
    icon: 'person'
  },
  atencion: {
    title: 'Atención',
    url: '/atencion',
    icon: 'list'
  },
  notificaciones: {
    title: 'Notificaciones',
    url: '/notificaciones-listado',
    icon: 'notifications'
  },
  formularios: {
    title: 'Formularios',
    url: '/reportes-inicio',
    icon: 'md-clipboard'
  },
  usuarios: {
    title: 'Usuarios',
    url: '/usuarios-listado',
    icon: 'contacts'
  },
  configuraciones: {
    title: 'Configuraciones',
    url: '/configuraciones',
    icon: 'settings'
  },
  aeronaves_0_hs:{
    title: 'Mantenimiento',
    url: '/aeronaves-mantenimiento',
    icon: 'cog'
  },
  aeronaves_secretaria: {
    title: 'Aeronaves',
    url: '/list',
    icon: 'list'
  },
  aprobar_reservas: {
    title: 'Aprobar reservas',
    url: '/aprobar-reservas',
    icon: 'calendar'
  },
  pistas: {
    title: 'Pistas',
    url: '/pistas-listado',
    icon: 'remove-circle-outline'
  }
};

export const reservaColors = {
  red: {
    primary: '#ad2121',
    secondary: '#FAE3E3'
  },
  blue: {
    primary: '#1e90ff',
    secondary: '#D1E8FF'
  },
  yellow: {
    primary: '#f1c40f',
    secondary: '#FDF1BA'
  }
};


export const reservaEstados = [
  {
    "idEstadoReserva": 1,
    "descripcion": "PRERESERVA",
    color: {
      primary: '#f1c40f',
      secondary: '#FDF1BA'
    }
  },
  {
    "idEstadoReserva": 6,
    "descripcion": "FINALIZADO",
    color: {
      primary: '#7044ff',
      secondary: '#633ce0'
    }
  },
  {
    "idEstadoReserva": 3,
    "descripcion": "CANCELADO SECRETARIA",
    color: {
      primary: '#ad2121',
      secondary: '#FAE3E3'
    }
  },
  {
    "idEstadoReserva": 7,
    "descripcion": "CANCELADO SISTEMA",
    color: {
      primary: '#ad2121',
      secondary: '#FAE3E3'
    }
  },
  {
    "idEstadoReserva": 4,
    "descripcion": "CANCELADO TALLER",
    color: {
      primary: '#ad2121',
      secondary: '#FAE3E3'
    }
  },
  {
    "idEstadoReserva": 5,
    "descripcion": "MANTENIMIENTO",
    color: {
      primary: '#1e90ff',
      secondary: '#D1E8FF'
    }
  },
  {
    "idEstadoReserva": 2,
    "descripcion": "RESERVADO",
    color: {
      primary: '#10dc60',
      secondary: '#0ec254'
    }
  }
];
