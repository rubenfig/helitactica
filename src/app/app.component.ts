import { Component } from '@angular/core';

import {Events, Platform} from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import {UserService} from './services/user.service';
import {menuPerfil} from './utils/utils';
import {Router} from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html'
})

export class AppComponent {
  public currentPages = [];

  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    public events: Events,
    private userService: UserService,
    private router: Router
  ) {
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
      // OneSignal Code start:
      // Enable to debug issues:
      // window["plugins"].OneSignal.setLogLevel({logLevel: 4, visualLevel: 4});

      const notificationOpenedCallback = (jsonData) => {
        console.log('notificationOpenedCallback: ' + JSON.stringify(jsonData));
      };

      if (this.platform.is('android')) {
        window['plugins'].OneSignal
          .startInit('374280ff-1dc2-4dd9-9a3a-530627c62d2d', 'N2IyNTRjODYtNjk4MS00Zjg4LTk4MTctYTU1ODgxMTNlNjlm')
          .handleNotificationOpened(notificationOpenedCallback)
          .endInit();
      } else if (this.platform.is('ios')) {
        window['plugins'].OneSignal
          .startInit('a10607e5-4f14-4129-9922-61dd289b5442')
          .handleNotificationOpened(notificationOpenedCallback)
          .endInit();
      }
      this.events.subscribe('userLoggedIn', () => {
        this.updatePages();
      });
      this.updatePages();
    });
  }

  logout() {
    this.userService.logout().subscribe((res: any) => {
      if (res.estado === 0 ) {
        this.router.navigate(['/login'], { replaceUrl: true });
      }
    });
  }

  updatePages() {
    const user = this.userService.getUser();
    const accionesActuales = [];
    if (user) {
      for (const accion of user.acciones) {
        if (menuPerfil.hasOwnProperty(accion)) {
          accionesActuales.push(menuPerfil[accion]);
        }
      }
      if (user.perfil === 'Taller' && accionesActuales.length === 0) {
        accionesActuales.push(menuPerfil['aeronaves_0_hs']);
        accionesActuales.push(menuPerfil['pistas']);
      }
      if (user.perfil === 'Secretaria') {
        accionesActuales.push(menuPerfil['pistas']);
        accionesActuales.push(menuPerfil['instructores']);
      }
    }
    this.currentPages = accionesActuales;
  }
}
