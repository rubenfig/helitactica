import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {LoadingController} from '@ionic/angular';
import {formatDate, Location} from '@angular/common';
import {AeronavesService} from '../../services/aeronaves.service';
import {ToastService} from '../../services/toast.service';
import moment from 'moment-timezone';

@Component({
  selector: 'app-crear-mantenimiento',
  templateUrl: './crear-mantenimiento.page.html',
  styleUrls: ['./crear-mantenimiento.page.scss'],
})
export class CrearMantenimientoPage implements OnInit {
  aeronaveElegida: { idAeronave: number, descripcion: string, costoPorPeriodo: number };
  mantenimiento: any = {
    inicio: new Date().toISOString(),
    fase: '3'
  };
  anho = (new Date()).getFullYear();
  user: any;
  loading: any;
  aeronaves: any;

  constructor(private route: ActivatedRoute,
              private location: Location,
              private aeronavesService: AeronavesService,
              private loadingController: LoadingController,
              private toast: ToastService) {
  }

  ngOnInit() {
    this.route.paramMap.subscribe(
      (params) => {
        this.aeronaveElegida = JSON.parse(params.get('aeronave'));
        this.listarAviones();
      }
    );
  }

  onChangeAeronave(event) {
    this.aeronaveElegida = event.detail.value;
  }

  compareById(o1, o2) {
    return o1.idAeronave === o2.idAeronave;
  }

  async presentLoading() {
    this.loading = await this.loadingController.create({
      message: 'Cargando...'
    });
    await this.loading.present();
  }

  async listarAviones() {
    await this.presentLoading();
    this.aeronavesService.listar().subscribe((response: any) => {
      this.loading.dismiss();
      this.aeronaves = response.lista;
      if (this.aeronaves.length > 0) {
        this.aeronaveElegida = this.aeronaves[0];
      }
      console.log(this.aeronaves);
    }, err => {
      this.loading.dismiss();
      this.toast.showDefaultToast(err.message);
    });
  }

  async agendar() {
    if (!(this.mantenimiento.horas || this.mantenimiento.dias)) {
      this.toast.showDefaultToast('Debe especificar los días y horas');
      return;
    }
    await this.presentLoading();
    const mantenimiento = {...this.mantenimiento};
    // tslint:disable-next-line:radix
    const horas = new Date(mantenimiento.horas).getHours();
    const dias = new Date(mantenimiento.dias).getHours();
    console.log(horas);
    console.log(dias);
    mantenimiento.fin = moment(mantenimiento.inicio).add(dias, 'd').add(horas, 'hours');
    mantenimiento.inicio = formatDate(new Date(mantenimiento.inicio), 'yyyy-MM-dd HH:mm:ss', 'es');
    mantenimiento.fin = formatDate(mantenimiento.fin.toDate(), 'yyyy-MM-dd HH:mm:ss', 'es');
    mantenimiento.idAeronave = this.aeronaveElegida.idAeronave;
    delete mantenimiento.horas;
    delete mantenimiento.dias;
    console.log(JSON.stringify(mantenimiento));
    this.aeronavesService.agendarMantenimiento(mantenimiento).subscribe((res: any) => {
      this.loading.dismiss();
      console.log(res);
      if (res.estado === 0) {
        this.toast.showDefaultToast('Mantenimiento agendado exitosamente!');
        this.location.back();
      } else {
        this.toast.showDefaultToast(res.mensaje || 'Ocurrió un error indeterminado');
      }
    }, err => {
      console.log(err);
      this.loading.dismiss();
      this.toast.showDefaultToast(err.message);
    });
  }
}
