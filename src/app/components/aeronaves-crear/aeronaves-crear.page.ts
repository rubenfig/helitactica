import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {UserService} from '../../services/user.service';
import {Location} from '@angular/common';
import {FormBuilder, FormControl, Validators} from '@angular/forms';
import {LoadingController} from '@ionic/angular';
import {ToastService} from '../../services/toast.service';
import {AeronavesService} from '../../services/aeronaves.service';

@Component({
  selector: 'app-aeronaves-crear',
  templateUrl: './aeronaves-crear.page.html',
  styleUrls: ['./aeronaves-crear.page.scss'],
})
export class AeronavesCrearPage implements OnInit {
  aeronave: any;
  validationMessages = {
    descripcion: [{ type: 'required', message: 'El modelo es requerido.' }]
  };
  loading: any;
  idAeronave;
  editar = false;
  constructor(private route: ActivatedRoute,
              private userService: UserService,
              private aeronavesService: AeronavesService,
              private location: Location,
              private formBuilder: FormBuilder,
              private loadingController: LoadingController,
              private toast: ToastService) { }
  ngOnInit() {
    this.aeronave = this.formBuilder.group({
      descripcion: new FormControl('', Validators.required),
      costoPorPeriodo: new FormControl(0),
      horasVueloDisponible: new FormControl(0)
    });
    this.route.paramMap.subscribe(
      (params) => {
        const aeronave = JSON.parse(params.get('aeronave'));
        console.log(aeronave);
        if (aeronave) {
          this.editar = true;
          this.aeronave.patchValue(aeronave);
          this.idAeronave = aeronave.idAeronave;
        }
      }
    );
  }

  async presentLoading() {
    this.loading = await this.loadingController.create({
      message: 'Cargando...'
    });
    await this.loading.present();
  }

  async guardar() {
    await this.presentLoading();
    const aeronave = {...this.aeronave.value};
    console.log(aeronave);
    let guardarFunc;
    if (this.editar) {
      aeronave.idAeronave = this.idAeronave;
      guardarFunc = this.aeronavesService.modificarAeronave(aeronave);
    } else {
      guardarFunc = this.aeronavesService.crearAeronave(aeronave);
    }
    guardarFunc.subscribe((res: any) => {
      this.loading.dismiss();
      console.log(res);
      if (res.estado === 0) {
        this.toast.showDefaultToast('Aeronave guardada exitosamente!');
        this.location.back();
      } else {
        this.toast.showDefaultToast(res.mensaje || 'Ocurrió un error indeterminado');
      }
    }, err => {
      console.log(err);
      this.loading.dismiss();
      this.toast.showDefaultToast(err.message);
    });
  }
}
