import {ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {AeronavesService} from '../../services/aeronaves.service';
import {LoadingController} from '@ionic/angular';
import {UserService} from '../../services/user.service';
import {ToastService} from '../../services/toast.service';

@Component({
  selector: 'app-pistas-listado',
  templateUrl: './pistas-listado.page.html',
  styleUrls: ['./pistas-listado.page.scss'],
})
export class PistasListadoPage implements OnInit {

  pistas = [];
  loading: any;
  perfil: string;
  constructor(private router: Router,
              private aeronavesService: AeronavesService,
              private loadingController: LoadingController,
              private userService: UserService,
              private toast: ToastService,
              private changeRef: ChangeDetectorRef) { }

  ngOnInit() {
    const userData = this.userService.getUser();
    if (userData && userData.perfil) {
      this.perfil = userData.perfil.toLowerCase();
    }
  }

  ionViewDidEnter() {
    this.listarPistas();
  }

  async presentLoading() {
    this.loading = await this.loadingController.create({
      message: 'Cargando...'
    });
    await this.loading.present();
  }

  async listarPistas() {
    await this.presentLoading();
    this.aeronavesService.listarPistas().subscribe((response: any) => {
      this.loading.dismiss();
      this.pistas = response.lista;
      this.changeRef.detectChanges();
      console.log(this.pistas);
    }, err => {
      this.loading.dismiss();
      this.toast.showDefaultToast(err.message);
    });
  }

  irACierrePista(pista) {
    this.router.navigate(['/agendar-cierre-pista', { pista: JSON.stringify(pista) }]);
  }

}
