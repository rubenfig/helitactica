import { Component, OnInit } from '@angular/core';
import {formatDate, Location} from '@angular/common';
import {LoadingController} from '@ionic/angular';
import {ToastService} from '../../services/toast.service';
import {InstructoresService} from '../../services/instructores.service';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-instructores-horario',
  templateUrl: './instructores-horario.page.html',
  styleUrls: ['./instructores-horario.page.scss'],
})
export class InstructoresHorarioPage implements OnInit {
  horario = {inicio: null, fin: null, dias: null, idInstructor: null};
  limpiar;
  loading: any;
  constructor(
    private loadingController: LoadingController,
    private instructoresService: InstructoresService,
    private location: Location,
    private toast: ToastService,
    private route: ActivatedRoute
  ) { }

  ngOnInit() {
    this.route.paramMap.subscribe(
      (params) => {
        const instructor = JSON.parse(params.get('instructor'));
        this.limpiar = params.get('limpiar') === 'true';
        this.horario.idInstructor = instructor.idInstructor;
      }
    );
  }

  async presentLoading() {
    this.loading = await this.loadingController.create({
      message: 'Cargando...'
    });
    await this.loading.present();
  }

  async guardar() {
    await this.presentLoading();
    const horario = {...this.horario};
    horario.inicio = formatDate(new Date(horario.inicio), 'HH:mm:ss', 'es');
    horario.fin =  formatDate(new Date(horario.fin), 'HH:mm:ss', 'es');
    horario.dias = horario.dias.map(d => {
      // tslint:disable-next-line:radix
      return parseInt(d);
    });
    console.log(JSON.stringify(horario));
    this.instructoresService.cargarHorario(horario, this.limpiar).subscribe((res: any) => {
      this.loading.dismiss();
      console.log(res);
      if (res.estado === 0) {
        this.toast.showDefaultToast(`Horario ${this.limpiar ? 'limpiado' : 'cargado'} exitosamente!`);
        this.location.back();
      } else {
        this.toast.showDefaultToast(res.mensaje || 'Ocurrió un error indeterminado');
      }
    }, err => {
      console.log(err);
      this.loading.dismiss();
      this.toast.showDefaultToast(err.message);
    });
  }
}
