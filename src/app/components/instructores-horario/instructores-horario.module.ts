import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { InstructoresHorarioPage } from './instructores-horario.page';

const routes: Routes = [
  {
    path: '',
    component: InstructoresHorarioPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [InstructoresHorarioPage]
})
export class InstructoresHorarioPageModule {}
