import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {LoadingController} from '@ionic/angular';
import {ToastService} from '../../services/toast.service';
import {ReservaService} from '../../services/reserva.service';
import {Location} from '@angular/common';
import {ReportesService} from '../../services/reportes.service';
import {reemplazarGuion} from '../../utils/utils';

@Component({
  selector: 'app-cargar-reporte',
  templateUrl: './cargar-reporte.page.html',
  styleUrls: ['./cargar-reporte.page.scss'],
})
export class CargarReportePage implements OnInit {
  idReporteTipo;
  parametros = [];
  idReserva;
  reemplazarGuion = reemplazarGuion;
  reserva;
  loading;
  constructor(
    private route: ActivatedRoute,
    private loadingController: LoadingController,
    private toast: ToastService,
    private location: Location,
    private reservaService: ReservaService,
    private reportesService: ReportesService,
  ) { }

  ngOnInit() {
    this.route.paramMap.subscribe(
      (params) => {
        this.idReporteTipo = params.get('idReporteTipo');
        this.parametros = JSON.parse(params.get('parametros'));
      }
    );
  }

  async presentLoading() {
    this.loading = await this.loadingController.create({
      message: 'Cargando...'
    });
    await this.loading.present();
  }

  async obtenerReserva() {
    await this.presentLoading();
    this.reservaService.obtenerReserva(this.idReserva).subscribe((response: any) => {
      this.loading.dismiss();
      console.log(response);
      if (response.estado === 0) {
        if (response.lista.length === 0) {
          this.reserva = null;
          this.toast.showDefaultToast('No se encontró la reserva');
        } else {
          this.reserva = response.lista[0];
        }
      } else {
        this.reserva = null;
        this.toast.showDefaultToast(response.mensaje || 'Ocurrió un error indeterminado');
      }
    }, err => {
      this.reserva = null;
      this.loading.dismiss();
      this.toast.showDefaultToast(err.message);
    });
  }

  async cargarReporte() {
    if (!this.reserva) {
      this.toast.showDefaultToast('Debe elegir una reserva existente');
      return;
    }
    await this.presentLoading();
    const reporte = {
      idReserva: this.reserva.idReserva,
      // tslint:disable-next-line:radix
      idReporteTipo: parseInt(this.idReporteTipo),
      valores: this.parametros
    };
    console.log(JSON.stringify(reporte));
    this.reportesService.cargarReporte(reporte).subscribe((res: any) => {
      this.loading.dismiss();
      console.log(res);
      if (res.estado === 0) {
        this.toast.showDefaultToast('Reporte cargado exitosamente!');
        this.location.back();
      } else {
        this.toast.showDefaultToast(res.mensaje || 'Ocurrió un error indeterminado');
      }
    }, err => {
      console.log(err);
      this.loading.dismiss();
      this.toast.showDefaultToast(err.message);
    });
  }
}
