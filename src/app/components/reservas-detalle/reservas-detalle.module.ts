import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import {RatingPopoverComponent, ReservasDetallePage} from './reservas-detalle.page';
import {IonicRatingModule} from 'ionic4-rating/dist';
import {ModalRechazoComponent} from '../aprobar-reservas/modal-rechazo.component';

const routes: Routes = [
  {
    path: '',
    component: ReservasDetallePage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes),
    IonicRatingModule
  ],
  declarations: [ReservasDetallePage, RatingPopoverComponent],
  entryComponents: [RatingPopoverComponent]
})
export class ReservasDetallePageModule {}
