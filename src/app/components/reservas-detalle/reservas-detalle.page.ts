import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {MisionesService} from '../../services/misiones.service';
import {ToastService} from '../../services/toast.service';
import {AlertController, Events, LoadingController, ModalController, NavParams, PopoverController} from '@ionic/angular';
import {ReservaService} from '../../services/reserva.service';
import {UserService} from '../../services/user.service';
import {Location} from '@angular/common';
import {ModalRechazoComponent} from '../aprobar-reservas/modal-rechazo.component';
import {reemplazarGuion} from '../../utils/utils';

@Component({
  selector: 'app-reservas-detalle',
  templateUrl: './reservas-detalle.page.html',
  styleUrls: ['./reservas-detalle.page.scss'],
})
export class ReservasDetallePage implements OnInit {
  @ViewChild('id') idElement: ElementRef;
  reserva: any = {};
  tiposRechazo = [];
  reemplazarGuion = reemplazarGuion;
  loading: any;
  calificado = false;
  perfil: string;
  idAlumno;
  constructor(private route: ActivatedRoute,
              private router: Router,
              private misionesService: MisionesService,
              private toast: ToastService,
              private location: Location,
              private reservaService: ReservaService,
              private alertController: AlertController,
              private loadingController: LoadingController,
              private modalController: ModalController,
              private userService: UserService,
              private popoverController: PopoverController,
              public events: Events) { }
  ngOnInit() {
    this.events.unsubscribe('reservaCalificada');
    this.events.subscribe('reservaCalificada', (data) => {
      this.calificarReserva(data);
    });
    this.route.paramMap.subscribe(
        (params) => {
          this.reserva = JSON.parse(params.get('reserva'));
          console.log(this.reserva);
          if (this.reserva.idMision) {
            this.obtenerMision();
          }
          if (this.reserva.calificacion) {
            this.calificado = true;
          }
          console.log(this.reserva);
        }
    );
    const userData = this.userService.getUser();
    if (userData && userData.perfil) {
      this.perfil = userData.perfil.toLowerCase();
      if (this.perfil.includes('alumno') && userData.alumno) {
        this.idAlumno = userData.alumno.idAlumno;
      }
    }
    this.listarTiposRechazo();
  }

  async listarTiposRechazo() {
    this.reservaService.listarRechazo().subscribe((response: any) => {
      this.tiposRechazo = response.lista;
      console.log(this.tiposRechazo);
    }, err => {
      this.loading.dismiss();
      this.toast.showDefaultToast(err.message);
    });
  }


  async presentPopover() {

    const popover = await this.popoverController.create({
      component: RatingPopoverComponent,
      componentProps: {
        reserva: this.reserva,
      },
      translucent: true
    });
    return await popover.present();
  }

  async presentLoading() {
    this.loading = await this.loadingController.create({
      message: 'Cargando...'
    });
    await this.loading.present();
  }

  irAlMapa() {
    this.router.navigate(['/mapa-reserva', {reserva: JSON.stringify(this.reserva)}]);
  }

  async obtenerMision() {
    this.presentLoading();
    this.misionesService.listar().subscribe((response: any) => {
      this.loading.dismiss();
      for (const mision of response.lista) {
        if (mision.idMision === this.reserva.idMision) {
          this.reserva.mision = mision;
        }
      }
    }, err => {
      this.loading.dismiss();
      this.toast.showDefaultToast(err.message);
    });
  }

  async calificarReserva(data) {
    await this.presentLoading();
    console.log(JSON.stringify(data));
    this.reservaService.calificarReserva(data).subscribe((res: any) => {
      this.loading.dismiss();
      console.log(res);
      if (res.estado === 0) {
        this.toast.showDefaultToast('Reserva calificada exitosamente!');
        this.reserva.calificacion = data.puntuacion;
        this.calificado = true;
      } else {
        this.toast.showDefaultToast(res.mensaje || 'Ocurrió un error indeterminado');
      }
    }, err => {
      console.log(err);
      this.loading.dismiss();
      this.toast.showDefaultToast(err.message);
    });
  }

  async mostrarAlertMotivo(reserva) {
    if (this.perfil === 'secretaria') {
      const modal = await this.modalController.create({
        component: ModalRechazoComponent,
        componentProps: {
          reserva,
          tiposRechazo: this.tiposRechazo
        },
        cssClass: 'modal-rechazo'

      });
      await modal.present();
      const {data} = await modal.onWillDismiss();
      if (data.reload) {
        this.location.back();
      }
      return;
    } else {
      this.rechazarReserva();
    }
  }

  async rechazarReserva() {
    const data = {
      idReserva: this.reserva.idReserva,
      idInstructor: this.reserva.instructor ? this.reserva.instructor.idInstructor : this.reserva.idInstructor,
      estadoReserva : 3,
      idMision : this.reserva.mision ? this.reserva.mision.idMision : this.reserva.idMision,
      motivoRechazo: 'Rechazo Alumno',
      tipoRechazo: 7
    };
    await this.presentLoading();
    console.log(JSON.stringify(data));
    this.reservaService.modificarEstado(data).subscribe((res: any) => {
      this.loading.dismiss();
      console.log(res);
      if (res.estado === 0) {
        this.toast.showDefaultToast('Reserva modificada exitosamente!');
        this.location.back();
      } else {
        this.toast.showDefaultToast(res.mensaje || 'Ocurrió un error indeterminado');
      }
    }, err => {
      console.log(err);
      this.loading.dismiss();
      this.toast.showDefaultToast(err.message);
    });
  }

}


@Component({
  selector: 'menu-alumno',
  template: '<ion-content padding>' +
    '<rating [(ngModel)]="calificacion" size="default"></rating>' +
    '<ion-item  margin-vertical>' +
      '<ion-label position="floating">Comentario</ion-label>' +
      '<ion-textarea autoGrow type="text" [(ngModel)]="comentario" name="comentario"></ion-textarea>' +
    '</ion-item>' +
    '<p style="text-align: center; font-size: : 9px;">Las calificaciones son anónimas</p>' +
    '<ion-button expand="block" (click)="calificar()">Calificar</ion-button></ion-content>',
  styleUrls: ['./reservas-detalle.page.scss'],
})
export class RatingPopoverComponent {
  calificacion: any;
  comentario: string;
  constructor(private popoverController: PopoverController, private navParams: NavParams, private events: Events ) {
  }

  calificar() {
    const data = {
      puntuacion: this.calificacion,
      observacion : this.comentario,
      idReserva : this.navParams.get('reserva').idReserva
    };
    if (!data.puntuacion) {
      delete data.puntuacion;
    }
    this.events.publish('reservaCalificada', data);
    this.popoverController.dismiss();
  }

}
