import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {UserService} from '../../services/user.service';
import {AlertController, LoadingController} from '@ionic/angular';
import {formatDate, Location} from '@angular/common';
import {ToastService} from '../../services/toast.service';
import {AlumnosService} from '../../services/alumnos.service';
import {CursosService} from '../../services/cursos.service';

@Component({
  selector: 'app-alumnos-editar',
  templateUrl: './alumnos-editar.page.html',
  styleUrls: ['./alumnos-editar.page.scss'],
})
export class AlumnosEditarPage implements OnInit {
  cursoElegido: {idCurso: number, descripcion: string};
  loading: any;
  cursos: any;
  alumno: any;
  constructor(private route: ActivatedRoute,
              private alumnoService: AlumnosService,
              private alertController: AlertController,
              private location: Location,
              private cursosService: CursosService,
              private loadingController: LoadingController,
              private toast: ToastService) { }
  ngOnInit() {
    this.route.paramMap.subscribe(
      (params) => {
        this.alumno = JSON.parse(params.get('alumno'));
      }
    );
    this.listarCursos();
  }

  onChangeCurso(event) {
    this.cursoElegido = event.detail.value;
  }

  compareById(o1, o2) {
    return o1.idCurso === o2.idCurso;
  }
  async presentLoading() {
    this.loading = await this.loadingController.create({
      message: 'Cargando...'
    });
    await this.loading.present();
  }

  async listarCursos() {
    await this.presentLoading();
    this.cursosService.listar().subscribe((response: any) => {
      this.loading.dismiss();
      this.cursos = response.lista;
      if (this.cursos.length > 0) {
        this.cursoElegido = this.cursos[0];
      }
      console.log(this.cursos);
    }, err => {
      this.loading.dismiss();
      this.toast.showDefaultToast(err.message);
    });
  }

  async guardarAlumno() {
    await this.presentLoading();
    const alumno = {
      idAlumno: this.alumno.idAlumno,
      fechaVencimientoPermiso: new Date(this.alumno.fechaVencimientoPermiso),
      cma: new Date(this.alumno.cma),
      idCurso: this.cursoElegido.idCurso,
      nroLicenciaVuelo: this.alumno.nroLicenciaVuelo
    };
    console.log(JSON.stringify(alumno));
    this.alumnoService.modificarAlumno(alumno).subscribe((res: any) => {
      this.loading.dismiss();
      console.log(res);
      if (res.estado === 0) {
        this.toast.showDefaultToast('Alumno editado exitosamente!');
        this.location.back();
      } else {
        this.toast.showDefaultToast(res.mensaje || 'Ocurrió un error indeterminado');
      }
    }, err => {
      console.log(err);
      this.loading.dismiss();
      this.toast.showDefaultToast(err.message);
    });
  }
}
