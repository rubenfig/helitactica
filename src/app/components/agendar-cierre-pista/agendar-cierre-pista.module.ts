import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { AgendarCierrePistaPage } from './agendar-cierre-pista.page';

const routes: Routes = [
  {
    path: '',
    component: AgendarCierrePistaPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [AgendarCierrePistaPage]
})
export class AgendarCierrePistaPageModule {}
