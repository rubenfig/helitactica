import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {formatDate, Location} from '@angular/common';
import {AeronavesService} from '../../services/aeronaves.service';
import {AlertController, LoadingController} from '@ionic/angular';
import {ToastService} from '../../services/toast.service';
import moment from 'moment-timezone';

@Component({
  selector: 'app-agendar-cierre-pista',
  templateUrl: './agendar-cierre-pista.page.html',
  styleUrls: ['./agendar-cierre-pista.page.scss'],
})
export class AgendarCierrePistaPage implements OnInit {

  aeronaveElegida: [];
  cierrePista: any = {
    id: null,
    inicio: new Date().toISOString(),
    fin: new Date().toISOString()
  };
  anho = (new Date()).getFullYear();
  user: any;
  loading: any;
  aeronaves: any;

  constructor(private route: ActivatedRoute,
              private alertController: AlertController,
              private location: Location,
              private aeronavesService: AeronavesService,
              private loadingController: LoadingController,
              private toast: ToastService) {
  }

  ngOnInit() {
    this.route.paramMap.subscribe(
      (params) => {
        this.cierrePista.id = JSON.parse(params.get('pista')).id;
        this.listarAviones();
      }
    );
  }

  onChangeAeronave(event) {
    console.log(event);
    console.log(this.aeronaveElegida);
    // this.aeronaveElegida = event.detail.value;
  }

  compareById(o1, o2) {
    return o1.idAeronave === o2.idAeronave;
  }

  async presentLoading() {
    this.loading = await this.loadingController.create({
      message: 'Cargando...'
    });
    await this.loading.present();
  }

  async listarAviones() {
    await this.presentLoading();
    this.aeronavesService.listar().subscribe((response: any) => {
      this.loading.dismiss();
      this.aeronaves = response.lista;
      console.log(this.aeronaves);
    }, err => {
      this.loading.dismiss();
      this.toast.showDefaultToast(err.message);
    });
  }

  async agendar() {
    const pista = {...this.cierrePista};
    if (moment(pista.inicio).isAfter(moment(pista.fin))) {
      this.toast.showDefaultToast('La fecha inicio no puede ser después de fecha fin');
      return;
    }
    pista.inicio = formatDate(new Date(pista.inicio), 'yyyy-MM-dd HH:mm:ss', 'es');
    pista.fin = formatDate(new Date(pista.fin), 'yyyy-MM-dd HH:mm:ss', 'es');
    pista.aeronaves = this.aeronaveElegida.map((avion: {idAeronave: any}) => avion.idAeronave);
    console.log(JSON.stringify(pista));
    const alert = await this.alertController.create({
      header: 'Está seguro?',
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
            console.log('Confirm Cancel');
          }
        }, {
          text: 'Confirmar',
          handler: async (data) => {
            await this.presentLoading();
            this.aeronavesService.confirmarCierrePista(pista).subscribe((res: any) => {
              this.loading.dismiss();
              console.log(res);
              if (res.estado === 0) {
                this.toast.showDefaultToast('Cierre de pista agendado exitosamente!');
                // this.location.back();
              } else {
                this.toast.showDefaultToast(res.mensaje || 'Ocurrió un error indeterminado');
              }
            }, err => {
              console.log(err);
              this.loading.dismiss();
              this.toast.showDefaultToast(err.message);
            });
          }
        }
      ]
    });

    await alert.present();
  }
}
