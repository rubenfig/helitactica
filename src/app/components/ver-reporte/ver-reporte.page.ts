import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {LoadingController} from '@ionic/angular';
import {ToastService} from '../../services/toast.service';
import {ReservaService} from '../../services/reserva.service';
import {Location} from '@angular/common';
import {ReportesService} from '../../services/reportes.service';
import {reemplazarGuion} from '../../utils/utils';

@Component({
  selector: 'app-cargar-reporte',
  templateUrl: './ver-reporte.page.html',
  styleUrls: ['./ver-reporte.page.scss'],
})
export class VerReportePage implements OnInit {
  idReporteTipo;
  idReserva;
  reporte;
  reemplazarGuion = reemplazarGuion;
  reserva;
  loading;
  constructor(
    private route: ActivatedRoute,
    private loadingController: LoadingController,
    private toast: ToastService,
    private location: Location,
    private reservaService: ReservaService,
    private reportesService: ReportesService,
  ) { }

  ngOnInit() {
    this.route.paramMap.subscribe(
      (params) => {
        this.reporte = JSON.parse(params.get('reporte'));
        console.log(this.reporte);
        this.obtenerReserva();
      }
    );
  }

  async presentLoading() {
    this.loading = await this.loadingController.create({
      message: 'Cargando...'
    });
    await this.loading.present();
  }

  async obtenerReserva() {
    await this.presentLoading();
    this.reservaService.obtenerReserva(this.reporte.idReserva).subscribe((response: any) => {
      this.loading.dismiss();
      console.log(response);
      if (response.estado === 0) {
        if (response.lista.length === 0) {
          this.reserva = null;
          this.toast.showDefaultToast('No se encontró la reserva');
        } else {
          this.reserva = response.lista[0];
        }
      } else {
        this.reserva = null;
        this.toast.showDefaultToast(response.mensaje || 'Ocurrió un error indeterminado');
      }
    }, err => {
      this.reserva = null;
      this.loading.dismiss();
      this.toast.showDefaultToast(err.message);
    });
  }

  descargarReporte() {
    const url = this.reportesService.descargarReporte(this.reporte.idReporte);
    window.open(url, '_system', 'location=yes');
  }
}
