import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import {ActivatedRoute, Router} from '@angular/router';
import {LoadingController} from '@ionic/angular';
import {ToastService} from '../../services/toast.service';
import {UserService} from '../../services/user.service';

@Component({
  selector: 'app-cambiar-password',
  templateUrl: './cambiar-password.page.html',
  styleUrls: ['./cambiar-password.page.scss'],
})
export class CambiarPasswordPage implements OnInit {
  passActual: string;
  passNuevo: string;
  confirmPassword: string;
  loading: any;

  constructor(private route: ActivatedRoute,
              private user: UserService,
              public router: Router,
              private location: Location,
              public toastProvider: ToastService,
              public loadingCtrl: LoadingController,
  ) { }

  ngOnInit() {
    this.route.paramMap.subscribe(
        (params) => this.passActual = params.get('passActual')
    );
  }
  // Attempt to login in through our User service
  async cambiarPassword() {
    if (this.passNuevo === this.confirmPassword) {
      this.loading = await this.loadingCtrl.create({
        message: 'Cambiando contraseña...',
      });
      this.loading.present();
      this.user.cambiarPassword({passActual: this.passActual, passNuevo: this.passNuevo}).subscribe((resp: any) => {
        this.loading.dismiss();
        if (resp.estado === 0) {
          this.user.logout();
          this.location.back();
        } else {
          this.toastProvider.showDefaultToast(resp.mensaje);
        }
      }, (err) => {
        this.loading.dismiss();
        // this.navCtrl.setRoot(MainPage);
        console.log(err);
        this.toastProvider.showDefaultToast('Ocurrió un error indeterminado!');
      });
    } else {
      this.toastProvider.showDefaultToast('Las contraseñas no coinciden');
    }

  }


}
