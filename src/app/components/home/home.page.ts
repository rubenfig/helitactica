import { Component } from '@angular/core';
import {AlumnosService} from '../../services/alumnos.service';
import {LoadingController} from '@ionic/angular';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  loading;

  constructor(
      private alumnosService: AlumnosService,
      private loadingController: LoadingController
  ) {}

  async presentLoading() {
    this.loading = await this.loadingController.create({
      message: 'Cargando...'
    });
    await this.loading.present();
  }
  async listarAlumnos() {
    await this.presentLoading();
  }

}
