import { Component, OnInit } from '@angular/core';
import {UserService} from '../../services/user.service';
import {ReservaService} from '../../services/reserva.service';
import {ActivatedRoute, Router} from '@angular/router';
import {reservaEstados} from '../../utils/utils';
import {reemplazarGuion} from '../../utils/utils';

@Component({
  selector: 'app-reservas-alumno',
  templateUrl: './reservas-alumno.page.html',
  styleUrls: ['./reservas-alumno.page.scss'],
})
export class ReservasAlumnoPage implements OnInit {
  reservas = [];
  reemplazarGuion = reemplazarGuion;

  constructor(private route: ActivatedRoute,
              private router: Router,
              private userService: UserService,
              private reservaService: ReservaService) { }
  ngOnInit() {
    this.route.paramMap.subscribe(
      (params) => {
        const idAlumno = params.get('idAlumno');
        if (idAlumno) {
          this.reservaService.listarTodasReservasAlumno(idAlumno).subscribe( (res: any) => {
            if (res.estado === 0) {
              for (const reserva of res.lista) {
                for (const estado of reservaEstados) {
                  if (estado.idEstadoReserva === reserva.estadoReserva) {
                      reserva.estado = estado;
                  }
                }
              }
              this.reservas = res.lista;
            }
            console.log(res.lista);
          });
        }
      }
    );
  }

  goToReservaDetalle(reserva) {
      console.log(reserva);
      this.router.navigate(['/reservas-detalle', { reserva: JSON.stringify(reserva) }]);
  }

}
