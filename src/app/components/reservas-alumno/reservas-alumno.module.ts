import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { ReservasAlumnoPage } from './reservas-alumno.page';

const routes: Routes = [
  {
    path: '',
    component: ReservasAlumnoPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [ReservasAlumnoPage]
})
export class ReservasAlumnoPageModule {}
