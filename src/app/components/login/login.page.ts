import { Component, OnInit } from '@angular/core';
import {Events, LoadingController, NavController, Platform} from '@ionic/angular';
import {UserService} from '../../services/user.service';
import {ToastService} from '../../services/toast.service';
import {SplashScreen} from '@ionic-native/splash-screen/ngx';
import {StatusBar} from '@ionic-native/status-bar/ngx';
import {Router} from '@angular/router';
import {KeychainTouchId} from '@ionic-native/keychain-touch-id/ngx';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  account: { pass: string, user: string, uid: string, terminal: string} = {
    pass: '',
    user: '',
    uid: 'asdasd',
    terminal: 'android'
  };
  recordar = false;
  loading: any;
  hasKeychain = false;
  hasKeychainPassword = false;
  touchHabilitado = false;
  touchAsked = false;

  // Our translated text strings
  private loginErrorString: string;

  constructor(public user: UserService,
              public toastProvider: ToastService,
              public loadingCtrl: LoadingController,
              private userService: UserService,
              private platform: Platform,
              private splashScreen: SplashScreen,
              private statusBar: StatusBar,
              private router: Router,
              private touch: KeychainTouchId,
              public events: Events) {

    this.loginErrorString = 'Ocurrió un error al loguear';
    this.initializeApp();
  }

  initializeApp() {
    this.touchHabilitado = localStorage.getItem('touchHabilitado') !== 'false';
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
      if (this.userService.isRecordado()) {
        this.cargarUsuario();
        this.recordar = true;
      }
      this.touch.isAvailable().then((result) => {
        console.log('has FP');
        this.hasKeychain = true;
        this.touch.has('HELIAPP').then(() => {
          console.log('has password');
          this.hasKeychainPassword = true;
        });
      });
    });
  }

  cargarUsuario() {
    const usuario = this.userService.getUser();
    if (usuario && usuario.usuario && usuario.usuario.usuario) {
      this.account.user = usuario.usuario.usuario;
      this.account.pass = usuario.usuario.pass;
    }
  }

  async onPasswordTouch() {
    console.log(this.touchHabilitado);
    if (this.hasKeychain && this.hasKeychainPassword && !this.touchAsked && this.touchHabilitado) {
      this.touch.verify('HELIAPP', 'Por favor, introduzca su huella').then(password => {
        this.cargarUsuario();
        this.account.pass = password;
        this.doLogin();
      }).catch((err) => {
        console.log(err);
        this.toastProvider.showDefaultToast('Ocurrió un error al escanear su huella');
      });
      this.touchAsked = true;
    }
  }
  ionViewWillEnter() {
    this.hasKeychainPassword = false;
    this.hasKeychain = false;
    this.account.pass = '';
    this.initializeApp();
  }

  // Attempt to login in through our User service
  async doLogin() {
    this.loading = await this.loadingCtrl.create({
      message: 'Iniciando sesión...',
    });
    this.loading.present();
    if (window['plugins'].OneSignal && window['plugins'].OneSignal.getIds) {
      console.log('has onesignal');
      window['plugins'].OneSignal.getIds((ids) => {
        this.account.uid = ids.userId;
        this.account.terminal = this.platform.is('android') ? 'android' : 'ios';
        // this.account.terminal = 'ios';
        this.finishLogin();
      }, error => {
        this.finishLogin();
      });
    } else {
      console.log('doesnt have onesignal');
      this.finishLogin();
    }
  }

  finishLogin() {
    this.user.login(this.account).subscribe((resp: any) => {
      this.loading.dismiss();
      console.log(resp);
      if (resp.status === 0 ) {
        this.userService.setRecordado(this.recordar);
        this.events.publish('userLoggedIn');
        if (resp.usuario.primerPassword) {
          this.router.navigate(['/cambiar-password', {passActual: this.account.pass}]);
        } else {
          if (this.hasKeychain && !this.hasKeychainPassword && this.touchHabilitado) {
            this.touch.save('HELIAPP', this.account.pass);
            localStorage.setItem('touchHabilitado', 'true');
          }
          const taller = resp.perfil && resp.perfil.toLowerCase().includes('taller');
          this.router.navigate([taller ? '/aeronaves-mantenimiento' : '/reservas']);
          // const perfil = resp.perfil;
          // console.log(perfil);
          // if (perfil && perfil.toLowerCase().includes('alumno')) {
          //   this.router.navigate(['/reservas']);
          // } else {
          //   this.router.navigate(['/alumnos-listado']);
          // }
        }
      } else {
        this.toastProvider.showDefaultToast(resp.mensaje);
      }
    }, (err) => {
      this.loading.dismiss();
      // this.navCtrl.setRoot(MainPage);
      console.log(err);
      this.toastProvider.showDefaultToast('Ocurrió un error indeterminado!');
    });
  }

  ngOnInit() {
  }

}
