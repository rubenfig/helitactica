import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {Location} from '@angular/common';
import {UserService} from '../../services/user.service';
import {ReservaService} from '../../services/reserva.service';
import {reservaEstados} from '../../utils/utils';
import {AlertController, LoadingController, ModalController} from '@ionic/angular';
import {ToastService} from '../../services/toast.service';
import {InstructoresService} from '../../services/instructores.service';
import {MisionesService} from '../../services/misiones.service';
import {ModalRechazoComponent} from './modal-rechazo.component';
import {reemplazarGuion} from '../../utils/utils';
@Component({
  selector: 'app-aprobar-reservas',
  templateUrl: './aprobar-reservas.page.html',
  styleUrls: ['./aprobar-reservas.page.scss'],
})
export class AprobarReservasPage implements OnInit {
  reservas = [];
  reemplazarGuion = reemplazarGuion;
  loading: any;
  instructores = [];
  tiposRechazo = [];
  misiones = [];
  constructor(private route: ActivatedRoute,
              private router: Router,
              private userService: UserService,
              private alertController: AlertController,
              private toast: ToastService,
              private modalController: ModalController,
              private loadingController: LoadingController,
              private location: Location,
              private reservaService: ReservaService,
              private instructoresService: InstructoresService,
              private misionesService: MisionesService) { }
  ngOnInit() {
    this.listarPendientes();
    this.listarMisiones();
    this.listarInstructores();
    this.listarTiposRechazo();
  }

  async presentLoading() {
    this.loading = await this.loadingController.create({
      message: 'Cargando...'
    });
    await this.loading.present();
  }

  async listarPendientes() {
    await this.presentLoading();
    this.reservaService.listarReservasPendientes().subscribe( (res: any) => {
      console.log(res);
      this.loading.dismiss();
      if (res.estado === 0) {
        for (const reserva of res.lista) {
          for (const estado of reservaEstados) {
            if (estado.idEstadoReserva === reserva.estadoReserva) {
              reserva.estado = estado;
            }
          }
        }
        this.reservas = res.lista;
      } else {
        this.toast.showDefaultToast(res.mensaje || 'Ocurrió un error indeterminado');
      }
      console.log(res.lista);
    });
  }

  async mostrarAlertMotivo(reserva) {
    const modal = await this.modalController.create({
      component: ModalRechazoComponent,
      componentProps: {
        reserva,
        tiposRechazo: this.tiposRechazo
      },
      cssClass: 'modal-rechazo'

    });
    await modal.present();
    const { data } = await modal.onWillDismiss();
    if (data.reload) {
      this.listarPendientes();
    }
    return;
  }

  async modificarReserva(reserva, estado, motivo?) {
    const data = {
      idReserva: reserva.idReserva,
      idInstructor: reserva.instructor ? reserva.instructor.idInstructor : reserva.idInstructor,
      estadoReserva : estado,
      idMision : reserva.mision ? reserva.mision.idMision : reserva.idMision,
      motivoRechazo: motivo
    };
    await this.presentLoading();
    console.log(JSON.stringify(data));
    this.reservaService.modificarEstado(data).subscribe((res: any) => {
      this.loading.dismiss();
      console.log(res);
      if (res.estado === 0) {
        this.toast.showDefaultToast('Reserva modificada exitosamente!');
        this.listarPendientes();
      } else {
        this.toast.showDefaultToast(res.mensaje || 'Ocurrió un error indeterminado');
      }
    }, err => {
      console.log(err);
      this.loading.dismiss();
      this.toast.showDefaultToast(err.message);
    });
  }

  onChangeInstructor(event, reserva) {
    reserva.idInstructor = event.detail.value;
  }
  onChangeMision(event, reserva) {
    reserva.idMision = event.detail.value;
  }

  compareByIdInstructor(o1, o2) {
    return o1.idInstructor === o2.idInstructor;
  }

  compareByIdMision(o1, o2) {
    return o1.idMision === o2.idMision;
  }


  async listarMisiones() {
    this.misionesService.listar().subscribe((response: any) => {
      this.misiones = response.lista;
      console.log(this.misiones);
    }, err => {
      this.loading.dismiss();
      this.toast.showDefaultToast(err.message);
    });
  }

  async listarTiposRechazo() {
    this.reservaService.listarRechazo().subscribe((response: any) => {
      this.tiposRechazo = response.lista;
      console.log(this.tiposRechazo);
    }, err => {
      this.loading.dismiss();
      this.toast.showDefaultToast(err.message);
    });
  }

  async listarInstructores() {
    this.instructoresService.listar().subscribe((response: any) => {
      this.instructores = response.lista;
      console.log(this.instructores);
    }, err => {
      this.loading.dismiss();
      this.toast.showDefaultToast(err.message);
    });
  }

}
