import {Component, Input} from '@angular/core';
import {LoadingController, ModalController, NavParams} from '@ionic/angular';
import {ReservaService} from '../../services/reserva.service';
import {ToastService} from '../../services/toast.service';

@Component({
  selector: 'modal-rechazo',
  template: '' +
    '<ion-header>' +
    '  <ion-toolbar color="primary">' +
    '    <ion-buttons slot="start">' +
    '      <ion-button (click)="closeModal()">' +
    '                <ion-icon name="arrow-back"></ion-icon>' +
  '         </ion-button>' +
    '    </ion-buttons>' +
    '    <ion-title>Rechazar reserva</ion-title>' +
    '  </ion-toolbar>' +
    '</ion-header>' +
    '<ion-content padding >' +
    '<form>' +
    '<ion-list>' +
    '<ion-item no-margin no-padding lines="none">' +
    '<ion-label position="floating">Tipo de rechazo</ion-label>' +
    '<ion-select placeholder="Seleccione un tipo" [compareWith]="compareById" name="tipoRechazo"' +
    '                      [(ngModel)]="tipoRechazo"' +
    '                      cancelText="Cancelar" okText="Ok">' +
    '<ion-select-option *ngFor="let tipo of tiposRechazo" [value]="tipo">{{tipo.descripcion}}</ion-select-option>' +
    '</ion-select>' +
    '</ion-item>' +
    '<ion-item no-margin no-padding lines="none">' +
    '<ion-label position="floating">Motivo</ion-label>' +
    '<ion-input type="text" [(ngModel)]="motivoRechazo" name="motivo"></ion-input>' +
    '<p>Obs: Se aplicarán recargos al cancelar una reserva</p>' +
    '</ion-item>' +
    '<div padding>' +
    '<ion-button color="primary" expand="block" (click)="rechazarReserva()">Rechazar reserva</ion-button>' +
    '</div>' +
    '</ion-list>' +
    '</form>' +
    '</ion-content>',
  styleUrls: ['./aprobar-reservas.page.scss']
})
export class ModalRechazoComponent {
  tipoRechazo;
  @Input() tiposRechazo;
  @Input() reserva;
  motivoRechazo;
  loading: any;
  constructor(navParams: NavParams,
              private modalCtrl: ModalController,
              private loadingController: LoadingController,
              private toast: ToastService,
              private reservaService: ReservaService) {
    this.tiposRechazo = navParams.get('tiposRechazo');
    this.tipoRechazo = this.tiposRechazo[0];
    this.reserva = navParams.get('reserva');
  }

  closeModal() {
    this.modalCtrl.dismiss();
  }

  compareById(o1, o2) {
    return o1.idTipoRechazo === o2.idTipoRechazo;
  }

  async presentLoading() {
    this.loading = await this.loadingController.create({
      message: 'Cargando...'
    });
    await this.loading.present();
  }


  async rechazarReserva() {
    const data = {
      idReserva: this.reserva.idReserva,
      idInstructor: this.reserva.instructor ? this.reserva.instructor.idInstructor : this.reserva.idInstructor,
      estadoReserva : 3,
      idMision : this.reserva.mision ? this.reserva.mision.idMision : this.reserva.idMision,
      motivoRechazo: this.motivoRechazo,
      tipoRechazo: this.tipoRechazo.idTipoRechazo
    };
    await this.presentLoading();
    console.log(JSON.stringify(data));
    this.reservaService.modificarEstado(data).subscribe((res: any) => {
      this.loading.dismiss();
      console.log(res);
      if (res.estado === 0) {
        this.toast.showDefaultToast('Reserva modificada exitosamente!');
        this.modalCtrl.dismiss({reload: true});
      } else {
        this.toast.showDefaultToast(res.mensaje || 'Ocurrió un error indeterminado');
      }
    }, err => {
      console.log(err);
      this.loading.dismiss();
      this.toast.showDefaultToast(err.message);
    });
  }

}
