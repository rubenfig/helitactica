import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { AprobarReservasPage } from './aprobar-reservas.page';
import {ModalRechazoComponent} from './modal-rechazo.component';

const routes: Routes = [
  {
    path: '',
    component: AprobarReservasPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [AprobarReservasPage],
  entryComponents: []
})
export class AprobarReservasPageModule {}
