import { Component, OnInit } from '@angular/core';
import {Subject} from 'rxjs';
import {CalendarEvent, CalendarView} from 'angular-calendar';
import {ReservaService} from '../../services/reserva.service';
import {UserService} from '../../services/user.service';
import {ActivatedRoute, Router} from '@angular/router';
import {reservaEstados} from '../../utils/utils';
import {AeronavesService} from '../../services/aeronaves.service';
import {LoadingController} from '@ionic/angular';
import {ToastService} from '../../services/toast.service';
import moment from 'moment-timezone';

@Component({
  selector: 'app-reservas-avion',
  templateUrl: './reservas-avion.page.html',
  styleUrls: ['./reservas-avion.page.scss'],
})
export class ReservasAvionPage implements OnInit  {
  aeronaveElegida: {idAeronave: number, descripcion: string};
  aeronaves: [];
  view: CalendarView = CalendarView.Week;
  perfil: string;
  viewDate: Date = new Date();
  currentHour = moment().hour();
  maxDate = moment().add(2, 'months');
  nextDisabled = false;
  fechaElegida = new Date().toISOString();
  refresh: Subject<any> = new Subject();
  now = new Date(2019, 5, 13, 7, 0);
  later = new Date(2019, 5, 13, 10, 0);
  events: CalendarEvent[] = [];
  loading: any;

  constructor(private reservaService: ReservaService,
              private userService: UserService,
              private router: Router,
              private route: ActivatedRoute,
              private aeronavesService: AeronavesService,
              private loadingController: LoadingController,
              private toast: ToastService) { }

  ngOnInit() {
    const userData = this.userService.getUser();
    if (userData && userData.perfil) {
      this.perfil = userData.perfil.toLowerCase();
    }
    this.route.paramMap.subscribe(
      (params) => {
        this.aeronaveElegida = JSON.parse(params.get('aeronave'));
        if (this.aeronaveElegida) {
          this.obtenerReservas(this.viewDate);
        }
        this.listarAviones();
      }
    );
  }

  async presentLoading() {
    if (this.loading) {
      this.loading.dismiss();
      this.loading = null;
    }
    this.loading = await this.loadingController.create({
      message: 'Cargando...',
      duration: 5000
    });
    await this.loading.present();
  }

  async listarAviones() {
    await this.presentLoading();
    this.aeronavesService.listar().subscribe((response: any) => {
      this.loading.dismiss();
      this.aeronaves = response.lista;
    }, err => {
      this.loading.dismiss();
      this.toast.showDefaultToast(err.message);
    });
  }

  isCurrentHour(date) {
    return moment(date).hour() === this.currentHour ? ' hora-actual' : '';
  }

  compareById(o1, o2) {
    return o1.idAeronave === o2.idAeronave;
  }

  onChangeAeronave(event) {
    this.aeronaveElegida = event.detail.value;
    this.obtenerReservas(this.viewDate);
  }

  cambiarFiltros() {
    this.viewDate = moment(this.fechaElegida, 'YYYY/MM/DD').toDate();
    this.obtenerReservas(this.viewDate);
  }

  obtenerReservas(date) {
    const desde = moment(date).hour(0).minutes(0).seconds(0).format('YYYY-MM-DD HH:mm:ss');
    const hasta = moment(date).hour(0).minutes(0).seconds(0).add(4, 'd').format('YYYY-MM-DD HH:mm:ss');
    this.nextDisabled = this.maxDate.isBefore(moment(date).add(4, 'd')) && this.perfil.includes('alumno');
    this.presentLoading();
    this.reservaService.listarReservasAvion(this.aeronaveElegida.idAeronave, desde, hasta).subscribe( (res: any) => {
      // tslint:disable-next-line:no-unused-expression
      this.loading && this.loading.dismiss();
      if (res.estado === 0) {
        const events = [];
        for (const reserva of res.lista) {
          const event = {
            start: moment(reserva.inicio).toDate(),
            end: moment(reserva.fin).toDate(),
            meta: reserva,
            draggable: false,
            title: '',
            color: null
          };
          for (const estado of reservaEstados) {
            if (estado.idEstadoReserva === reserva.estadoReserva) {
              event.title = estado.descripcion;
              event.color = estado.color;
              reserva.estado = estado;
            }
          }
          events.push(event);
        }
        this.events = [...events];
        console.log(events);
      }
    }, err => {
      this.loading.dismiss();
    });
  }

  handleEvent(action: string, event: CalendarEvent): void {
    this.router.navigate(['/reservas-detalle', { reserva: JSON.stringify(event.meta) }]);
  }

  crearReserva(e) {
    this.router.navigate(['/reservas-crear', {fecha: e.date}]);
  }

  dateIsValid(date: Date): boolean {
    return date <= new Date();
  }

  beforeMonthViewRender({ body }: { body: any }): void {
    body.forEach(day => {
      if (!this.dateIsValid(day.date)) {
        day.cssClass = 'cal-disabled';
      }
    });
  }
}
