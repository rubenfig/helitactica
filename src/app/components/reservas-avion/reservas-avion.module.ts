import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { ReservasAvionPage } from './reservas-avion.page';
import {CalendarModule} from 'angular-calendar';

const routes: Routes = [
  {
    path: '',
    component: ReservasAvionPage
  }
];

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        RouterModule.forChild(routes),
        CalendarModule
    ],
  declarations: [ReservasAvionPage]
})
export class ReservasAvionPageModule {}
