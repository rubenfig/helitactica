import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';

@Component({
  selector: 'app-reportes-inicio',
  templateUrl: './reportes-inicio.page.html',
  styleUrls: ['./reportes-inicio.page.scss'],
})
export class ReportesInicioPage implements OnInit {
  idReserva;
  constructor(private router: Router) { }

  ngOnInit() {
  }

  goToFormularios() {
    this.router.navigate(['/tipos-reportes-listado']);
  }

  buscarReporte() {
    this.router.navigate(['/reportes', {idReserva: this.idReserva}]);
  }

}
