import { Component, OnInit } from '@angular/core';
import {NotificacionesService} from '../../services/notificaciones.service';
import {LoadingController} from '@ionic/angular';
import {ToastService} from '../../services/toast.service';
import {UserService} from '../../services/user.service';
import {ReservaService} from '../../services/reserva.service';
import {Router} from '@angular/router';
import {reservaEstados} from '../../utils/utils';

@Component({
  selector: 'app-notificaciones-listado',
  templateUrl: './notificaciones-listado.page.html',
  styleUrls: ['./notificaciones-listado.page.scss'],
})
export class NotificacionesListadoPage  implements OnInit {

  loading;
  notificaciones = [];
  idUsuario;
  constructor(
    private notificacionesService: NotificacionesService,
    private loadingController: LoadingController,
    private toast: ToastService,
    private userService: UserService,
    private reservaService: ReservaService,
    private router: Router
  ) {
    const userData = this.userService.getUser();
    this.idUsuario = userData.usuario.idUsuario;
  }

  async presentLoading() {
    this.loading = await this.loadingController.create({
      message: 'Cargando...'
    });
    await this.loading.present();
  }

  async listarNotificaciones() {
    await this.presentLoading();
    this.notificacionesService.listarNotificaciones(this.idUsuario).subscribe((response: any) => {
      this.loading.dismiss();
      console.log(response);
      this.notificaciones = response.lista;
    }, err => {
      this.loading.dismiss();
      this.toast.showDefaultToast(err.message);
    });
  }

  async verReserva(reserva) {
    await this.presentLoading();
    this.reservaService.obtenerReserva(reserva).subscribe((response: any) => {
      this.loading.dismiss();
      console.log(response);
      if (response.estado === 0) {
        if (response.lista.length === 0) {
          this.toast.showDefaultToast('No se encontró la reserva');
        } else {
          const reservaObtenida = response.lista[0];
          for (const estado of reservaEstados) {
            if (estado.idEstadoReserva === reservaObtenida.estadoReserva) {
              reservaObtenida.estado = estado;
            }
          }
          this.router.navigate(['/reservas-detalle', { reserva: JSON.stringify(reservaObtenida) }]);
        }
      } else {
        this.toast.showDefaultToast(response.mensaje || 'Ocurrió un error indeterminado');
      }
    }, err => {
      this.loading.dismiss();
      this.toast.showDefaultToast(err.message);
    });
  }

  ngOnInit() {
    this.listarNotificaciones();
  }
}
