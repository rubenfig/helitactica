import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { NotificacionesListadoPage } from './notificaciones-listado.page';

const routes: Routes = [
  {
    path: '',
    component: NotificacionesListadoPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [NotificacionesListadoPage]
})
export class NotificacionesListadoPageModule {}
