import { Component, OnInit } from '@angular/core';
import {AlertController, LoadingController} from '@ionic/angular';
import {ToastService} from '../../services/toast.service';
import {UserService} from '../../services/user.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-usuarios-listado',
  templateUrl: './usuarios-listado.page.html',
  styleUrls: ['./usuarios-listado.page.scss'],
})
export class UsuariosListadoPage implements OnInit {
  loading;
  usuarios = [];
  filtro = '';
  buscando = false;
  constructor(
    private userService: UserService,
    private loadingController: LoadingController,
    private alertController: AlertController,
    private toast: ToastService,
    private router: Router
  ) {}

  async presentLoading() {
    this.loading = await this.loadingController.create({
      message: 'Cargando...'
    });
    await this.loading.present();
  }

  mostrarSearchBar() {
    this.buscando = !this.buscando;
    if (!this.buscando) {
      this.filtro = '';
      this.listarUsuarios();
    }
  }

  ionViewWillEnter() {
    this.listarUsuarios();
  }

  async listarUsuarios() {
    await this.presentLoading();
    this.userService.listarUsuarios(this.filtro).subscribe((response: any) => {
      this.loading.dismiss();
      console.log(response);
      this.usuarios = response.lista;
      if (response.estado !== 0) {
        this.toast.showDefaultToast(response.mensaje);
      }
    }, err => {
      this.loading.dismiss();
      this.toast.showDefaultToast(err.message);
    });
  }

  async mostrarAlertPassword(usuario) {
    const alert = await this.alertController.create({
      header: 'Resetear la contraseña del usuario?',
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
            console.log('Confirm Cancel');
          }
        }, {
          text: 'Ok',
          handler: (data) => {
            this.resetearPassword(usuario);
          }
        }
      ]
    });

    await alert.present();
  }

  async resetearPassword(usuario) {
    console.log(usuario);
    this.loading = await this.loadingController.create({
      message: 'Reseteando contraseña...',
    });
    this.loading.present();
    this.userService.resetearPassword({usuario: usuario.usuario}).subscribe((resp: any) => {
      this.loading.dismiss();
      if (resp.estado === 0) {
        this.toast.showDefaultToast('Se reseteó la contraseña del usuario');
      } else {
        this.toast.showDefaultToast(resp.mensaje);
      }
    }, (err) => {
      this.loading.dismiss();
      // this.navCtrl.setRoot(MainPage);
      console.log(err);
      this.toast.showDefaultToast('Ocurrió un error indeterminado!');
    });
  }

  async habilitarUsuario(usuario) {
    await this.presentLoading();
    const data = {
      idUsuario: usuario.idUsuario,
      habilitado: usuario.habilitado
    };
    console.log(data);
    this.userService.habilitarUsuario(data).subscribe((response: any) => {
      this.loading.dismiss();
      console.log(response);
      if (response.estado !== 0) {
        this.toast.showDefaultToast(response.mensaje);
      } else {
        this.toast.showDefaultToast('Se editó el estado del usuario');
      }
    }, err => {
      this.loading.dismiss();
      this.toast.showDefaultToast(err.message);
    });
  }

  goToEditar(usuario) {
    this.router.navigate(['/usuarios-crear', { usuario: JSON.stringify(usuario) }]);
  }

  ngOnInit() {

  }

}
