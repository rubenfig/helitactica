import { Component, OnInit } from '@angular/core';
import {UserService} from '../../services/user.service';
import {Router} from '@angular/router';
import {reemplazarGuion} from '../../utils/utils';


@Component({
  selector: 'app-perfil',
  templateUrl: './perfil.page.html',
  styleUrls: ['./perfil.page.scss'],
})
export class PerfilPage implements OnInit {
  perfil: {
    saldoDisponible: string,
    idAlumno: string,
    fechaVencimientoPermiso: any,
    ultimoVuelo: Date,
    idUsuario: number,
    horasVuelo: number,
    direccion: string,
    idCurso: number,
    documento: string,
    habilitado: boolean,
    nombreCompleto: string,
    curso: any,
    nroLicenciaVuelo: string,
    cma: string
  };
  reemplazarGuion = reemplazarGuion;

  constructor(private userService: UserService,
              private router: Router) { }

  ngOnInit() {
    const userData = this.userService.getUser();
    console.log(userData);
    this.perfil = userData.alumno;
    this.perfil.nombreCompleto = userData.usuario.nombreCompleto;
  }

  goToReservas() {
    this.router.navigate(['/reservas-alumno', {idAlumno: this.perfil.idAlumno}]);
  }


  goToReportes() {
    this.router.navigate(['/reportes', {idAlumno: this.perfil.idAlumno}]);
  }

}
