import {ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {Subject} from 'rxjs';
import {CalendarEvent, CalendarView} from 'angular-calendar';
import {ReservaService} from '../../services/reserva.service';
import {UserService} from '../../services/user.service';
import {Router} from '@angular/router';
import {reservaEstados} from '../../utils/utils';
import moment from 'moment-timezone';
import {LoadingController} from '@ionic/angular';

@Component({
  selector: 'app-reservas',
  templateUrl: './reservas.page.html',
  styleUrls: ['./reservas.page.scss'],
})
export class ReservasPage implements OnInit {
  idAlumno: number;
  idUsuario: number;
  view: CalendarView = CalendarView.Week;
  perfil: string;
  viewDate: Date = new Date();
  currentHour = moment().hour();
  maxDate = moment().add(2, 'months');
  checkChanges = false;
  fechaElegida = new Date().toISOString();
  refresh: Subject<any> = new Subject();
  nextDisabled = false;
  events: CalendarEvent[] = [];
  loading: any;
  constructor(private reservaService: ReservaService,
              private userService: UserService,
              private router: Router,
              private loadingController: LoadingController,
              private changeRef: ChangeDetectorRef
  ) { }

  ngOnInit() {
    const userData = this.userService.getUser();
    if (userData && userData.perfil) {
      this.perfil = userData.perfil.toLowerCase();
      this.idUsuario = userData.usuario.idUsuario;
      if (this.perfil && this.perfil.includes('alumno') && userData.alumno ) {
        this.idAlumno = userData.alumno.idAlumno;
      }
    }
  }

  async presentLoading() {
    if (this.loading) {
      this.loading.dismiss();
      this.loading = null;
    }
    this.loading = await this.loadingController.create({
      message: 'Cargando...',
      duration: 5000
    });
    await this.loading.present();
  }

  ionViewWillEnter() {
    this.listarReservas(this.viewDate);
  }

  cambiarFiltros() {
    if (this.checkChanges) {
      this.viewDate = moment(this.fechaElegida, 'YYYY/MM/DD');
      this.listarReservas(this.viewDate);
    }
  }

  isCurrentHour(date) {
    return moment(date).hour() === this.currentHour ? ' hora-actual' : '';
  }

  listarReservas(date) {
    if (this.perfil) {
      const desde = moment(date).hour(0).minutes(0).seconds(0).format('YYYY-MM-DD HH:mm:ss');
      const hasta = moment(date).hour(0).minutes(0).seconds(0).add(4, 'd').format('YYYY-MM-DD HH:mm:ss');
      let listarFunc;
      if (this.perfil && this.perfil.includes('alumno')) {
        listarFunc = this.reservaService.listarReservasAlumno(this.idAlumno, desde, hasta);
      } else if (this.perfil && this.perfil.includes('instructor')) {
        listarFunc = this.reservaService.listarReservasInstructor(this.idUsuario, desde, hasta);
      } else {
        listarFunc = this.reservaService.listarReservas(desde, hasta);
      }
      this.currentHour = moment().hour();
      this.nextDisabled = this.maxDate.isBefore(moment(date).add(4, 'd')) && this.perfil.includes('alumno');
      this.presentLoading();
      listarFunc.subscribe((res: any) => {
        // tslint:disable-next-line:no-unused-expression
        this.loading && this.loading.dismiss();
        if (res.estado === 0) {
          const events = [];
          for (const reserva of res.lista) {
            const event = {
              start: moment(reserva.inicio).toDate(),
              end: moment(reserva.fin).toDate(),
              meta: reserva,
              draggable: false,
              title: '',
              color: null
            };
            for (const estado of reservaEstados) {
              if (estado.idEstadoReserva === reserva.estadoReserva) {
                event.title = estado.descripcion;
                event.color = estado.color;
                reserva.estado = estado;
              }
            }
            events.push(event);
          }
          this.events = [...events];
          this.changeRef.detectChanges();
          console.log(events);
        }
        if (!this.checkChanges) {
          this.checkChanges = true;
        }
      }, err => {
        this.loading.dismiss();
        if (!this.checkChanges) {
          this.checkChanges = true;
        }
      });
    }
  }

  handleEvent(action: string, event: CalendarEvent): void {
    this.router.navigate(['/reservas-detalle', { reserva: JSON.stringify(event.meta) }]);
  }

  crearReserva(e) {
    if (!this.perfil.includes('instructor')) {
      this.router.navigate(['/reservas-crear', {fecha: e.date}]);
    }
  }

}
