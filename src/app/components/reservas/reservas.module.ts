import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { ReservasPage } from './reservas.page';
import {CalendarCommonModule, CalendarWeekModule} from 'angular-calendar';

const routes: Routes = [
  {
    path: '',
    component: ReservasPage
  }
];

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        RouterModule.forChild(routes),
        CalendarWeekModule,
        CalendarCommonModule
    ],
  declarations: [ReservasPage]
})
export class ReservasPageModule {}
