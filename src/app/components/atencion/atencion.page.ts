import { Component, OnInit } from '@angular/core';
import {UserService} from '../../services/user.service';
import {LoadingController} from '@ionic/angular';
import {ToastService} from '../../services/toast.service';

@Component({
  selector: 'app-atencion',
  templateUrl: './atencion.page.html',
  styleUrls: ['./atencion.page.scss'],
})
export class AtencionPage implements OnInit {
  atencion = {nombre: '', correo: '', telefono: ''};
  loading: any;
  constructor(private userService: UserService,
              private loadingController: LoadingController,
              private toast: ToastService) { }

  ngOnInit() {
    this.obtenerContacto();
  }

  async presentLoading() {
    this.loading = await this.loadingController.create({
      message: 'Cargando...'
    });
    await this.loading.present();
  }

  async obtenerContacto() {
    await this.presentLoading();
    this.userService.obtenerContacto().subscribe((response: any) => {
      this.loading.dismiss();
      console.log(response);
      for (const contacto of response.lista) {
        switch (contacto.codigo) {
          case 'CONTACTO_CORREO':
            this.atencion.correo = contacto.valor;
            break;
          case 'CONTACTO_NUMERO':
            this.atencion.telefono = contacto.valor;
            break;
          case 'CONTACTO_DATOS':
            this.atencion.nombre = contacto.valor;
            break;
        }
      }
      // this.atencion = response.;
    }, err => {
      this.loading.dismiss();
      this.toast.showDefaultToast(err.message);
    });
  }
}
