import { Component, OnInit } from '@angular/core';
import {Platform} from '@ionic/angular';
import {KeychainTouchId} from '@ionic-native/keychain-touch-id/ngx';

@Component({
  selector: 'app-configuraciones',
  templateUrl: './configuraciones.page.html',
  styleUrls: ['./configuraciones.page.scss'],
})
export class ConfiguracionesPage implements OnInit {
  hasKeychain = false;
  hasKeychainPassword = false;
  touchHabilitado = false;
  constructor(
    private platform: Platform,
    private touch: KeychainTouchId
  ) { }

  ngOnInit() {
    this.touchHabilitado = localStorage.getItem('touchHabilitado') !== 'false';
    this.platform.ready().then(() => {
      this.touch.isAvailable().then((result) => {
        console.log('has FP');
        this.hasKeychain = true;
        this.touch.has('HELIAPP').then(() => {
          console.log('has password');
          this.hasKeychainPassword = true;
        });
      });
    });
  }

  cambiarHuella() {
    console.log(this.touchHabilitado);
    localStorage.setItem('touchHabilitado', this.touchHabilitado ? 'true' : 'false');
  }

}
