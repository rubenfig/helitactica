import { Component, OnInit } from '@angular/core';
import {icon, Map, Marker, marker, latLng, polyline, tileLayer} from 'leaflet';
import {ActivatedRoute} from '@angular/router';
import {LoadingController} from '@ionic/angular';
import {ReservaService} from '../../services/reserva.service';
import {ToastService} from '../../services/toast.service';
const iconRetinaUrl = 'leaflet/marker-icon-2x.png';
const iconUrl = 'leaflet/marker-icon.png';
const shadowUrl = 'leaflet/marker-shadow.png';
const iconDefault = icon({
  iconRetinaUrl,
  iconUrl,
  shadowUrl,
  iconSize: [25, 41],
  iconAnchor: [12, 41],
  popupAnchor: [1, -34],
  tooltipAnchor: [16, -28],
  shadowSize: [41, 41]
});
Marker.prototype.options.icon = iconDefault;

@Component({
  selector: 'app-mapa-reserva',
  templateUrl: './mapa-reserva.page.html',
  styleUrls: ['./mapa-reserva.page.scss'],
})
export class MapaReservaPage implements OnInit {
  map: Map;
  currentLat = 1.0;
  currentLng = 1.0;
  zoomLevel = 16;
  reserva;
  posiciones = [];
  loading: any;

  constructor(private route: ActivatedRoute,
              private toast: ToastService,
              private loadingController: LoadingController,
              private reservaService: ReservaService) {
  }

  ngOnInit() {
    this.route.paramMap.subscribe(
      (params) => {
        this.reserva = JSON.parse(params.get('reserva'));
        console.log(this.reserva);
        this.obtenerPosiciones();
      }
    );
  }

  ionViewDidEnter() {
  }

  obtenerPosiciones() {
    this.presentLoading();
    this.reservaService.listarPosiciones(this.reserva.idReserva).subscribe( (res: any) => {
      this.loadmap();
      // tslint:disable-next-line:no-unused-expression
      this.loading && this.loading.dismiss();
      if (res.estado === 0) {
        console.log(res);
        try {
          const markers = [];
          for (const posicion of res.lista) {
            markers.push(latLng(posicion.lat, posicion.lon));
            marker([posicion.lat, posicion.lon])
              .bindTooltip(`Altura: ${posicion.altitude} - Velocidad: ${posicion.speed}`, {direction: 'top'})
              .addTo(this.map);
          }
          console.log('here');
          console.log(markers);
          const polylineObj = polyline(markers).addTo(this.map);
          console.log('here2');
          this.map.fitBounds(polylineObj.getBounds());
          if (markers.length === 0) {
            this.toast.showDefaultToast('No se encontraron datos de localización');
          }
        } catch (e) {
          console.log(e);
        }
      }
    }, err => {
      this.loading.dismiss();
    });
  }

  async presentLoading() {
    if (this.loading) {
      this.loading.dismiss();
      this.loading = null;
    }
    this.loading = await this.loadingController.create({
      message: 'Cargando...',
      duration: 5000
    });
    await this.loading.present();
  }

  loadmap() {
    this.map = new Map('map').setView([this.currentLat, this.currentLng], this.zoomLevel);
    tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
      // tslint:disable-next-line
      attribution: 'Map data &copy; <a '
        + 'href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a '
        + 'href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery '
        + '© <a href="https://www.mapbox.com/">Mapbox</a>',
      maxZoom: 18
    }).addTo(this.map);
  }
}
