import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {UserService} from '../../services/user.service';
import {AlertController, LoadingController} from '@ionic/angular';
import {ToastService} from '../../services/toast.service';
import {AeronavesService} from '../../services/aeronaves.service';
import {ReservaService} from '../../services/reserva.service';
import {formatDate, Location} from '@angular/common';
import moment from 'moment-timezone';
import {AlumnosService} from '../../services/alumnos.service';
@Component({
  selector: 'app-reservas-crear',
  templateUrl: './reservas-crear.page.html',
  styleUrls: ['./reservas-crear.page.scss'],
})
export class ReservasCrearPage implements OnInit {
  aeronaveElegida: {idAeronave: number, descripcion: string, costoPorPeriodo: number};
  reserva: any = {
    fechaReserva: new Date(),
    bloque: '1'
  };
  alumnos = [];
  alumnoElegido: any;
  user: any;
  loading: any;
  perfil: string;
  aeronaves: any;
  alumno = '';
  constructor(private route: ActivatedRoute,
              private userService: UserService,
              private alumnosService: AlumnosService,
              private reservaService: ReservaService,
              private alertController: AlertController,
              private location: Location,
              private aeronavesService: AeronavesService,
              private loadingController: LoadingController,
              private toast: ToastService) { }
  ngOnInit() {
    this.user = this.userService.getUser();
    this.perfil = this.user.perfil.toLowerCase();
    if (this.perfil.includes('secretaria')) {
      this.listarAlumnos();
    } else {
      this.alumno = this.user.usuario.nombreCompleto;
      if (this.user && this.user.alumno && this.user.alumno.idAlumno) {
        this.reserva.idAlumno = this.user.alumno.idAlumno;
      }
    }
    console.log(this.user);
    console.log(this.reserva);
    this.route.paramMap.subscribe(
      (params) => {
        const fecha = params.get('fecha');
        this.reserva.fechaReserva = fecha;
        this.reserva.horaInicio = fecha;
      }
    );
    this.listarAviones();
  }

  onChangeAeronave(event) {
    this.aeronaveElegida = event.detail.value;
  }

  onChangeAlumno(event) {
    this.alumnoElegido = event.detail.value;
    this.reserva.idAlumno = this.alumnoElegido.idAlumno;
  }

  onChangeBloque(event) {
    this.reserva.bloque = event.detail.value;
  }
  compareById(o1, o2) {
    return o1.idAeronave === o2.idAeronave;
  }

  compareByIdAlumno(o1, o2) {
    return o1.idAlumno === o2.idAlumno;
  }
  async presentLoading() {
    this.loading = await this.loadingController.create({
      message: 'Cargando...'
    });
    await this.loading.present();
  }

  async listarAviones() {
    await this.presentLoading();
    this.aeronavesService.listar().subscribe((response: any) => {
      this.loading.dismiss();
      this.aeronaves = response.lista;
      if (this.aeronaves.length > 0) {
        this.aeronaveElegida = this.aeronaves[0];
      }
      console.log(this.aeronaves);
    }, err => {
      this.loading.dismiss();
      this.toast.showDefaultToast(err.message);
    });
  }

  async listarAlumnos() {
    this.alumnosService.listar().subscribe((response: any) => {
      this.alumnos = response.lista;
      if (this.alumnos.length > 0) {
        this.alumnoElegido = this.alumnos[0];
      }
      console.log(this.alumnos);
    }, err => {
      this.toast.showDefaultToast(err.message);
    });
  }

  async mostrarAlert() {
    try {
      const fecha = formatDate(new Date(this.reserva.fechaReserva), 'dd/MM/yyyy', 'es');
      const fechaDesde = new Date(this.reserva.horaInicio);
      // tslint:disable-next-line:radix
      const momentHasta = moment(fechaDesde).add(parseInt(this.reserva.bloque) * 90, 'minutes');
      // tslint:disable-next-line:radix
      const costo = this.aeronaveElegida.costoPorPeriodo * parseInt(this.reserva.bloque);
      const desde = formatDate(fechaDesde, 'HH:mm', 'es');
      const hasta = formatDate(momentHasta.toDate(), 'HH:mm', 'es');
      console.log(this.user);
      const alert = await this.alertController.create({
        header: 'Crear la reserva?',
        message: `<p><strong>Día: </strong> ${fecha}</p>` +
          `<p><strong>Desde: </strong> ${desde}</p>` +
          `<p><strong>Hasta: </strong> ${hasta}</p>` +
          `<p><strong>Aeronave: </strong> ${this.aeronaveElegida.descripcion}</p>` +
          `<p><strong>Costo: </strong> ${costo}</p>`,
        buttons: [
          {
            text: 'Cancelar',
            role: 'cancel',
            cssClass: 'secondary',
            handler: () => {
              console.log('Confirm Cancel');
            }
          }, {
            text: 'Confirmar',
            handler: (data) => {
              this.reservar();
            }
          }
        ]
      });

      await alert.present();
    } catch (e) {
      alert('Por favor, elija un bloque y aeronave');
    }
  }

  async reservar() {
    await this.presentLoading();
    const reserva = {...this.reserva};
    reserva.fechaReserva = formatDate(new Date(reserva.fechaReserva), 'yyyy-MM-dd HH:mm:ss', 'es');
    reserva.horaInicio =  formatDate(new Date(reserva.horaInicio), 'yyyy-MM-dd HH:mm:ss', 'es');
    reserva.idAeronave = this.aeronaveElegida.idAeronave;
    if (!reserva.idAlumno) {
      this.toast.showDefaultToast('Se debe seleccionar un alumno para la reserva');
      this.loading.dismiss();
      return;
    }
    if (this.alumnoElegido.tipo === 'temporal') {
      if (!reserva.nombre || reserva.nombre.length === 0 ||
        !reserva.apellido || reserva.apellido.length === 0 ||
        !reserva.documento || reserva.documento.length === 0) {
        this.toast.showDefaultToast('Debe completar los datos del alumno para la reserva');
        this.loading.dismiss();
        return;
      }
    }
    console.log(JSON.stringify(reserva));
    this.reservaService.crearReserva(reserva).subscribe((res: any) => {
      this.loading.dismiss();
      console.log(res);
      if (res.estado === 0) {
        this.toast.showDefaultToast('Reserva creada exitosamente!');
        this.location.back();
      } else {
        this.toast.showDefaultToast(res.mensaje || 'Ocurrió un error indeterminado');
      }
    }, err => {
      console.log(err);
      this.loading.dismiss();
      this.toast.showDefaultToast(err.message);
    });
  }
}
