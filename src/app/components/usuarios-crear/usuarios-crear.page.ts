import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {UserService} from '../../services/user.service';
import {Location} from '@angular/common';
import {LoadingController} from '@ionic/angular';
import {ToastService} from '../../services/toast.service';
import {FormBuilder, FormControl, Validators} from '@angular/forms';
import {InstructoresService} from '../../services/instructores.service';

@Component({
  selector: 'app-usuarios-crear',
  templateUrl: './usuarios-crear.page.html',
  styleUrls: ['./usuarios-crear.page.scss'],
})
export class UsuariosCrearPage implements OnInit {
  perfilElegido: {idPerfil: number, descripcion: string};
  instructorElegido: {idInstructor: number, nombreCompleto: string};
  usuario: any;
  validationMessages = {
    usuario: [{ type: 'required', message: 'El nombre de usuario es requerido.' }],
    nombreCompleto: [{ type: 'required', message: 'El nombre es requerido.' }],
    correo: [
      { type: 'required', message: 'El correo es requerido.' },
      { type: 'pattern', message: 'Debe ser un correo válido.' }
    ],
    perfil: [{ type: 'required', message: 'El perfil es requerido.' }],
    instructor: [{ type: 'required', message: 'El instructor es requerido.' }],
    nroLicenciaVuelo: [{ type: 'required', message: 'La licencia es requerida.' }]
  };
  loading: any;
  perfiles: any = [];
  instructores: any = [];
  idUsuario: number;
  alumno = '';
  editar = false;
  constructor(private route: ActivatedRoute,
              private userService: UserService,
              private instructoresService: InstructoresService,
              private location: Location,
              private formBuilder: FormBuilder,
              private loadingController: LoadingController,
              private toast: ToastService) { }
  ngOnInit() {
    this.usuario = this.formBuilder.group({
      nombreCompleto: new FormControl('', Validators.required),
      usuario: new FormControl('', Validators.required),
      perfil: new FormControl(null, Validators.required),
      correo: new FormControl('', Validators.compose([
        Validators.required,
        Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')
      ])),
      direccion: new FormControl(''),
      documento: new FormControl(''),
      nroLicenciaVuelo: new FormControl(''),
      instructor: new FormControl('')
    });
    this.route.paramMap.subscribe(
      (params) => {
        const usuario = JSON.parse(params.get('usuario'));
        console.log(usuario);
        if (usuario) {
          this.editar = true;
          this.usuario.patchValue(usuario);
          this.perfilElegido = {idPerfil: usuario.idPerfil, descripcion: usuario.descripcionPerfil};
          this.idUsuario = usuario.idUsuario;
          this.usuario.patchValue({perfil: this.perfilElegido});
        }
        this.listarPerfiles();
        this.listarInstructores();
      }
    );
  }

  onChangePerfil(event) {
    this.perfilElegido = event.detail.value;
    if (this.perfilElegido.descripcion.toLowerCase().includes('alumno') && !this.editar) {
      this.usuario.get('nroLicenciaVuelo').setValidators([Validators.required]);
    } else {
      this.usuario.get('nroLicenciaVuelo').setValidators(null);

    }
    this.usuario.get('nroLicenciaVuelo').updateValueAndValidity();
  }

  onChangeInstructor(event) {
    this.instructorElegido = event.detail.value;
    if (this.instructorElegido.nombreCompleto.toLowerCase().includes('instructor') && !this.editar) {
      this.usuario.get('instructor').setValidators([Validators.required]);
    } else {
      this.usuario.get('instructor').setValidators(null);

    }
    this.usuario.get('instructor').updateValueAndValidity();
  }

  compareById(o1, o2) {
    return o1.idPerfil === o2.idPerfil;
  }

  compareByIdInstructor(o1, o2) {
    return o1.idInstructor === o2.idInstructor;
  }

  async presentLoading() {
    this.loading = await this.loadingController.create({
      message: 'Cargando...'
    });
    await this.loading.present();
  }

  async listarPerfiles() {
    await this.presentLoading();
    this.userService.listarPerfiles().subscribe((response: any) => {
      this.loading.dismiss();
      this.perfiles = response.lista;
      if (this.perfilElegido) {
        for (const perfil of this.perfiles) {
          if (this.perfilElegido === perfil.idPerfil) {
            console.log(this.perfilElegido);
            this.perfilElegido = perfil;
          }
        }
      }
      console.log(this.perfiles);
    }, err => {
      this.loading.dismiss();
      this.toast.showDefaultToast(err.message);
    });
  }

  async listarInstructores() {
    this.instructoresService.listar().subscribe((response: any) => {
      this.instructores = response.lista;
      console.log(this.instructores);
    }, err => {
      this.toast.showDefaultToast(err.message);
    });
  }


  async guardar() {
    await this.presentLoading();
    const usuario = {...this.usuario.value};
    usuario.idPerfil = this.perfilElegido.idPerfil;
    delete  usuario.perfil;
    delete  usuario.instructor;
    if (this.perfilElegido.descripcion === null || !this.perfilElegido.descripcion.toLowerCase().includes('alumno')) {
      delete usuario.documento;
      delete usuario.direccion;
      delete usuario.nroLicenciaVuelo;
    }
    if (this.perfilElegido.descripcion && this.perfilElegido.descripcion.toLowerCase().includes('instructor')) {
      usuario.idInstructor = this.instructorElegido.idInstructor;
    }
    console.log(usuario);
    let guardarFunc;
    if (this.editar) {
      usuario.idUsuario = this.idUsuario;
      guardarFunc = this.userService.modificarUsuario(usuario);
    } else {
      guardarFunc = this.userService.crearUsuario(usuario);
    }
    guardarFunc.subscribe((res: any) => {
      this.loading.dismiss();
      console.log(res);
      if (res.estado === 0) {
        this.toast.showDefaultToast('Usuario guardado exitosamente!');
        this.location.back();
      } else {
        this.toast.showDefaultToast(res.mensaje || 'Ocurrió un error indeterminado');
      }
    }, err => {
      console.log(err);
      this.loading.dismiss();
      this.toast.showDefaultToast(err.message);
    });
  }
}
