import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { InstructoresListadoPage } from './instructores-listado.page';

const routes: Routes = [
  {
    path: '',
    component: InstructoresListadoPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [InstructoresListadoPage]
})
export class InstructoresListadoPageModule {}
