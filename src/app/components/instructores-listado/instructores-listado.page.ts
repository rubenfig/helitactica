import { Component, OnInit } from '@angular/core';
import {InstructoresService} from '../../services/instructores.service';
import {LoadingController, PopoverController} from '@ionic/angular';
import {ToastService} from '../../services/toast.service';
import {UserService} from '../../services/user.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-instructores-listado',
  templateUrl: './instructores-listado.page.html',
  styleUrls: ['./instructores-listado.page.scss'],
})
export class InstructoresListadoPage implements OnInit {
  perfil = '';
  loading;
  instructores = [
  ];
  constructor(
      private instructoresService: InstructoresService,
      private userService: UserService,
      private router: Router,
      private loadingController: LoadingController,
      private toast: ToastService,
  ) {
    const userData = this.userService.getUser();
    if (userData && userData.perfil) {
      this.perfil = userData.perfil.toLowerCase();
    }
  }

  async presentLoading() {
    this.loading = await this.loadingController.create({
      message: 'Cargando...'
    });
    await this.loading.present();
  }

  async listarInstructores() {
    await this.presentLoading();
    this.instructoresService.listar().subscribe((response: any) => {
      this.loading.dismiss();
      console.log(response);
      this.instructores = response.lista;
    }, err => {
      this.loading.dismiss();
      this.toast.showDefaultToast(err.message);
    });
  }

  cargarHorario(instructor, limpiar) {
    this.router.navigate(['/instructores-horario', { instructor: JSON.stringify(instructor), limpiar }]);
  }

  ngOnInit() {
    this.listarInstructores();

  }
}
