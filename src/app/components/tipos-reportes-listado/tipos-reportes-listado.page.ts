import { Component, OnInit } from '@angular/core';
import {LoadingController} from '@ionic/angular';
import {ToastService} from '../../services/toast.service';
import {UserService} from '../../services/user.service';
import {Router} from '@angular/router';
import {ReportesService} from '../../services/reportes.service';

@Component({
  selector: 'app-tipos-reportes-listado',
  templateUrl: './tipos-reportes-listado.page.html',
  styleUrls: ['./tipos-reportes-listado.page.scss'],
})
export class TiposReportesListadoPage implements OnInit {

  loading;
  formularios = [];
  constructor(
    private reportesService: ReportesService,
    private loadingController: LoadingController,
    private toast: ToastService,
    private userService: UserService,
    private router: Router
  ) {
  }

  async presentLoading() {
    this.loading = await this.loadingController.create({
      message: 'Cargando...'
    });
    await this.loading.present();
  }

  async listarNotificaciones() {
    await this.presentLoading();
    this.reportesService.listarFormularios().subscribe((response: any) => {
      this.loading.dismiss();
      console.log(response);
      this.formularios = response.lista;
    }, err => {
      this.loading.dismiss();
      this.toast.showDefaultToast(err.message);
    });
  }

  async irACargarReporte(formulario) {
    await this.presentLoading();
    this.reportesService.listarParametros(formulario.idReporteTipo).subscribe((response: any) => {
      this.loading.dismiss();
      console.log(response);
      if (response.estado === 0) {
        if (response.lista.length === 0) {
          this.toast.showDefaultToast('No se encontró el formulario');
        } else {
          this.router.navigate(['/cargar-reporte', {
            parametros: JSON.stringify(response.lista),
            idReporteTipo: formulario.idReporteTipo
          }]);
        }
      } else {
        this.toast.showDefaultToast(response.mensaje || 'Ocurrió un error indeterminado');
      }
    }, err => {
      this.loading.dismiss();
      this.toast.showDefaultToast(err.message);
    });
  }

  ngOnInit() {
    this.listarNotificaciones();
  }
}
