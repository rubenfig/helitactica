import {ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {AeronavesService} from '../../services/aeronaves.service';
import {LoadingController} from '@ionic/angular';
import {UserService} from '../../services/user.service';
import {ToastService} from '../../services/toast.service';

@Component({
  selector: 'app-aeronaves-mantenimiento',
  templateUrl: './aeronaves-mantenimiento.page.html',
  styleUrls: ['./aeronaves-mantenimiento.page.scss'],
})
export class AeronavesMantenimientoPage implements OnInit {
  mantenimientos = [];
  loading: any;
  perfil: string;
  constructor(private router: Router,
              private aeronavesService: AeronavesService,
              private loadingController: LoadingController,
              private userService: UserService,
              private toast: ToastService,
              private changeRef: ChangeDetectorRef) { }

  ngOnInit() {
    const userData = this.userService.getUser();
    if (userData && userData.perfil) {
      this.perfil = userData.perfil.toLowerCase();
    }
  }

  ionViewDidEnter() {
    this.listarMantenimientos();
  }

  async presentLoading() {
    this.loading = await this.loadingController.create({
      message: 'Cargando...'
    });
    await this.loading.present();
  }

  async listarMantenimientos() {
    await this.presentLoading();
    this.aeronavesService.listarMantenimientos().subscribe((response: any) => {
      this.loading.dismiss();
      this.mantenimientos = response.lista;
      this.changeRef.detectChanges();
      console.log(this.mantenimientos);
    }, err => {
      this.loading.dismiss();
      this.toast.showDefaultToast(err.message);
    });
  }

  goToEditar(mantenimiento) {
    this.router.navigate(['/crear-mantenimiento', { mantenimiento: JSON.stringify(mantenimiento) }]);
  }

}
