import {ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {ReportesService} from '../../services/reportes.service';
import {LoadingController} from '@ionic/angular';
import {ActivatedRoute, Router} from '@angular/router';
import {ToastService} from '../../services/toast.service';

@Component({
  selector: 'app-reportes',
  templateUrl: './reportes.page.html',
  styleUrls: ['./reportes.page.scss'],
})
export class ReportesPage implements OnInit {
  loading;
  reportes = [];
  idAlumno;
  idReserva;
  fileTransfer;
  constructor(
    private reportesService: ReportesService,
    private loadingController: LoadingController,
    private route: ActivatedRoute,
    private toast: ToastService,
    private router: Router,
    private changeRef: ChangeDetectorRef
  ) {
    this.route.paramMap.subscribe(
      (params) => {
        this.idAlumno = JSON.parse(params.get('idAlumno'));
        if (!this.idAlumno) {
          this.idReserva = JSON.parse(params.get('idReserva'));
          this.listarReportesReserva();
        } else {
          this.listarReportesAlumno();
        }
      });
  }

  async presentLoading() {
    this.loading = await this.loadingController.create({
      message: 'Cargando...'
    });
    await this.loading.present();
  }

  async listarReportesAlumno() {
    await this.presentLoading();
    this.reportesService.listarReportesAlumno(this.idAlumno).subscribe((response: any) => {
      this.loading.dismiss();
      console.log(response);
      this.reportes = response;
      this.changeRef.detectChanges();
    }, err => {
      this.loading.dismiss();
      this.toast.showDefaultToast(err.message);
    });
  }

  async listarReportesReserva() {
    await this.presentLoading();
    this.reportesService.listarReportesReserva(this.idReserva).subscribe((response: any) => {
      this.loading.dismiss();
      console.log(response);
      this.reportes = response;
      this.changeRef.detectChanges();
    }, err => {
      this.loading.dismiss();
      this.toast.showDefaultToast(err.message);
    });
  }

  goToReporte(reporte) {
    const newReporte = {
      ...reporte,
      categorias: reporte.categorias.reduce(
        (result, c) => [...result,
          ...c.valores.map(
            v =>  {
              return {...v.parametro, valor: v.valor};
            }
          )], []
      )
    };
    console.log(newReporte);
    this.router.navigate(['/ver-reporte', {reporte: JSON.stringify(newReporte)}]);
  }
  ngOnInit(): void {
  }
}
