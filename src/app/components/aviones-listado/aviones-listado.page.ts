import {ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {AeronavesService} from '../../services/aeronaves.service';
import {LoadingController} from '@ionic/angular';
import {ToastService} from '../../services/toast.service';
import {UserService} from '../../services/user.service';

@Component({
  selector: 'app-aviones-listado',
  templateUrl: './aviones-listado.page.html',
  styleUrls: ['./aviones-listado.page.scss'],
})
export class AvionesListadoPage implements OnInit {
  aeronaves = [];
  aeronavesFiltradas = [];
  loading: any;
  filtro: string;
  buscando = false;
  perfil: string;
  constructor(private router: Router,
              private aeronavesService: AeronavesService,
              private loadingController: LoadingController,
              private userService: UserService,
              private toast: ToastService,
              private changeRef: ChangeDetectorRef) { }

  ngOnInit() {
    const userData = this.userService.getUser();
    if (userData && userData.perfil) {
      this.perfil = userData.perfil.toLowerCase();
    }
  }

  ionViewDidEnter() {
    this.listarAviones();
  }

  async presentLoading() {
    this.loading = await this.loadingController.create({
      message: 'Cargando...'
    });
    await this.loading.present();
  }

  mostrarSearchBar() {
    this.buscando = !this.buscando;
    if (!this.buscando) {
      this.filtro = '';
      this.listarAviones();
    }
  }

  filtrar() {
    this.aeronavesFiltradas = this.aeronaves.filter( aeronave => {
      return aeronave.descripcion && aeronave.descripcion.toLowerCase().includes(this.filtro.toLowerCase());
    });
    console.log(this.aeronavesFiltradas);
  }

  async listarAviones() {
    await this.presentLoading();
    let listarFunc;
    if (this.perfil.includes('alumno')) {
      listarFunc = this.aeronavesService.listar();
    } else {
      listarFunc = this.aeronavesService.listarTodos();
    }
    listarFunc.subscribe((response: any) => {
      this.loading.dismiss();
      this.aeronaves = response.lista;
      this.aeronavesFiltradas = response.lista;
      this.changeRef.detectChanges();
      console.log(this.aeronaves);
    }, err => {
      this.loading.dismiss();
      this.toast.showDefaultToast(err.message);
    });
  }

  async habilitarAeronave(aeronave) {
    await this.presentLoading();
    const data = {
      idAeronave: aeronave.idAeronave,
      habilitado: aeronave.habilitado
    };
    console.log(data);
    this.aeronavesService.modificarAeronave(data).subscribe((response: any) => {
      this.loading.dismiss();
      console.log(response);
      if (response.estado !== 0) {
        this.toast.showDefaultToast(response.mensaje);
      } else {
        this.toast.showDefaultToast('Se editó el estado de la aeronave');
      }
    }, err => {
      this.loading.dismiss();
      this.toast.showDefaultToast(err.message);
    });
  }

  goToReservasAvion(aeronave) {
    this.router.navigate(['/reservas-avion', {aeronave: JSON.stringify(aeronave)}]);
  }
  goToCrearMantenimiento(aeronave) {
    this.router.navigate(['/crear-mantenimiento', { aeronave: JSON.stringify(aeronave) }]);
  }

  goToEditar(aeronave) {
    this.router.navigate(['/aeronaves-crear', {aeronave: JSON.stringify(aeronave)}]);
  }

}
