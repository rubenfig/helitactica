import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, ParamMap} from '@angular/router';
import {switchMap} from 'rxjs/operators';

@Component({
  selector: 'app-resetear-password',
  templateUrl: './resetear-password.page.html',
  styleUrls: ['./resetear-password.page.scss'],
})
export class ResetearPasswordPage implements OnInit {
  oldPassword: string;
  newPassword: string;
  confirmPassword: string;

  constructor(private route: ActivatedRoute) { }

  ngOnInit() {
    this.route.paramMap.pipe(
        switchMap((params: ParamMap) =>
            this.oldPassword = params.get('oldPassword'))
    );
  }

}
