import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import {AlumnosListadoPage, MenuAlumnoComponent} from './alumnos-listado.page';

const routes: Routes = [
  {
    path: '',
    component: AlumnosListadoPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [AlumnosListadoPage, MenuAlumnoComponent],
  entryComponents: [MenuAlumnoComponent]
})
export class AlumnosListadoPageModule {}
