import {ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {AlumnosService} from '../../services/alumnos.service';
import {AlertController, LoadingController, PopoverController} from '@ionic/angular';
import {ToastService} from '../../services/toast.service';
import {Router} from '@angular/router';
import {reemplazarGuion} from '../../utils/utils';

@Component({
  selector: 'app-alumnos-listado',
  templateUrl: './alumnos-listado.page.html',
  styleUrls: ['./alumnos-listado.page.scss'],
})
export class AlumnosListadoPage implements OnInit {

  loading;
  alumnosFiltrados = [];
  alumnos = [];
  reemplazarGuion = reemplazarGuion;
  filtro = '';
  buscando = false;
  constructor(
    private alumnosService: AlumnosService,
    private loadingController: LoadingController,
    private toast: ToastService,
    private alertController: AlertController,
    private popoverController: PopoverController,
    private router: Router,
    private changeRef: ChangeDetectorRef
  ) {}

  async presentLoading() {
    this.loading = await this.loadingController.create({
      message: 'Cargando...'
    });
    await this.loading.present();
  }

  mostrarSearchBar() {
    this.buscando = !this.buscando;
    if (!this.buscando) {
      this.filtro = '';
      this.listarAlumnos();
    }
  }

  filtrar() {
    this.alumnosFiltrados = this.alumnos.filter( alumno => {
      return alumno.nombreCompleto && alumno.nombreCompleto.toLowerCase().includes(this.filtro.toLowerCase());
    });
    console.log(this.alumnosFiltrados);
  }

  async mostrarAlertSaldo(alumno, operacion) {
    console.log(alumno);
    const alert = await this.alertController.create({
      header: 'Agregar saldo al alumno',
      inputs: [
        {
          name: 'saldo',
          type: 'number',
          placeholder: 'Saldo a agregar'
        }
      ],
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
            console.log('Confirm Cancel');
          }
        }, {
          text: 'Confirmar',
          handler: (data) => {
            this.agregarSaldo(alumno, operacion === 1 ? data.saldo : data.saldo * -1);
          }
        }
      ]
    });
    await alert.present();
  }

  async agregarSaldo(alumno, saldo) {
    if ( alumno && saldo ) {
      this.presentLoading();
      this.alumnosService.agregarSaldo(alumno.idAlumno, saldo).subscribe((response: any) => {
        this.loading.dismiss();
        console.log(response);
        if (response.estado !== 0) {
          this.toast.showDefaultToast(response.mensaje);
        } else {
          this.toast.showDefaultToast('Se modificó el saldo exitosamente');
          this.listarAlumnos();
        }
      }, err => {
        this.loading.dismiss();
        this.toast.showDefaultToast(err.message);
      });
    } else {
      this.toast.showDefaultToast('Debe ingresar el saldo');
    }

  }

  async presentPopover(ev: any) {
    const popover = await this.popoverController.create({
      component: MenuAlumnoComponent,
      event: ev,
      translucent: true
    });
    return await popover.present();
  }

  async listarAlumnos() {
    await this.presentLoading();
    this.alumnosService.listar().subscribe((response: any) => {
      this.loading.dismiss();
      console.log(response);
      this.alumnos = response.lista;
      this.alumnosFiltrados = response.lista;
      this.changeRef.detectChanges();
    }, err => {
      this.loading.dismiss();
      this.toast.showDefaultToast(err.message);
    });
  }

  goToEditar(alumno) {
    this.router.navigate(['/alumnos-editar', { alumno: JSON.stringify(alumno) }]);
  }

  goToReportes(idAlumno) {
    this.router.navigate(['/reportes', {idAlumno}]);
  }

  ionViewDidEnter() {
    this.listarAlumnos();
  }

  ngOnInit() {
  }

}


@Component({
  selector: 'menu-alumno',
  template: '' +
      '<ion-list no-margin>' +
      '<ion-item (click)="close()" button>Agregar saldo</ion-item>' +
      '<ion-item (click)="close()" lines="none" button>Editar alumno</ion-item>' +
      '</ion-list>',
  styleUrls: ['./alumnos-listado.page.scss'],
})
export class MenuAlumnoComponent {
  constructor(private popoverController: PopoverController) {
  }

  close() {
    this.popoverController.dismiss();
  }

}
