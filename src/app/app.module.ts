import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import {IonicModule, IonicRouteStrategy, Platform} from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import {HTTP_INTERCEPTORS, HttpBackend, HttpClientModule, HttpXhrBackend} from '@angular/common/http';
import {NativeHttpBackend, NativeHttpFallback, NativeHttpModule} from 'ionic-native-http-connection-backend';
import {adapterFactory} from 'angular-calendar/date-adapters/date-fns';
import {CalendarModule, DateAdapter} from 'angular-calendar';
import { KeychainTouchId } from '@ionic-native/keychain-touch-id/ngx';
import localeEs from '@angular/common/locales/es';
import {registerLocaleData} from '@angular/common';
import {IonicRatingModule} from 'ionic4-rating/dist';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {AuthInterceptor} from './auth-interceptor';
import {AprobarReservasPage} from './components/aprobar-reservas/aprobar-reservas.page';
import {ModalRechazoComponent} from './components/aprobar-reservas/modal-rechazo.component';


registerLocaleData(localeEs, 'es');
@NgModule({
  declarations: [AppComponent, ModalRechazoComponent],
  entryComponents: [ModalRechazoComponent],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule, ReactiveFormsModule,
    IonicRatingModule, // Put ionic-rating module here
    IonicModule.forRoot(),
    AppRoutingModule,
    NativeHttpModule,
    CalendarModule.forRoot({
      provide: DateAdapter,
      useFactory: adapterFactory
    })
  ],
  providers: [
    StatusBar,
    SplashScreen,
    KeychainTouchId,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy },
    {provide: HttpBackend, useClass: NativeHttpFallback, deps: [Platform, NativeHttpBackend, HttpXhrBackend]},
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptor,
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
