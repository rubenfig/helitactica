import { Injectable } from '@angular/core';
import {ApiService} from './api.service';
import {HttpClient} from '@angular/common/http';
import {catchError, retry} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AeronavesService extends ApiService {
  path = 'aeronaves';
  constructor(public http: HttpClient) {
    super(http);
  }

  listarTodos() {
    return this.get(`${this.path}/todos`).pipe(
      retry(1),
      catchError(this.handleError)
    );
  }

  listarMantenimientos() {
    return this.get(`${this.path}/listaMantenimento/0`).pipe(
      retry(1),
      catchError(this.handleError)
    );
  }

  agendarMantenimiento(mantenimiento) {
    return this.post(`mantenimiento/nuevo`, mantenimiento).pipe(
      retry(1),
      catchError(this.handleError)
    );
  }

  modificarMantenimiento(mantenimiento) {
    return this.post(`mantenimiento/modificar`, mantenimiento).pipe(
      retry(1),
      catchError(this.handleError)
    );
  }

  crearAeronave(aeronave) {
    return this.post(`${this.path}/addAeronave`, aeronave).pipe(
      retry(1),
      catchError(this.handleError)
    );
  }

  modificarAeronave(aeronave) {
    return this.post(`${this.path}/modificarAeronave`, aeronave).pipe(
      retry(1),
      catchError(this.handleError)
    );
  }

  listarPistas() {
    return this.get(`pista/getPistas`).pipe(
      retry(1),
      catchError(this.handleError)
    );
  }

  agendarCierrePista(cierrePista) {
    return this.post(`pista/cierrePista`, cierrePista).pipe(
      retry(1),
      catchError(this.handleError)
    );
  }

  confirmarCierrePista(cierrePista) {
    return this.post(`pista/confirmarCierrePista`, cierrePista).pipe(
      retry(1),
      catchError(this.handleError)
    );
  }
}
