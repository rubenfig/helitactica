import { Injectable } from '@angular/core';
import {ApiService} from './api.service';
import {HttpClient} from '@angular/common/http';
import {catchError, retry} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ReportesService extends ApiService {
  path = 'reportes';
  constructor(public http: HttpClient) {
    super(http);
  }

  listarFormularios() {
    return this.get(`${this.path}/listarTiposReportes`).pipe(
      retry(1),
      catchError(this.handleError)
    );
  }

  listarParametros(id) {
    return this.get(
      `${this.path}/listarParametrosReportes?idReporteTipo=${id}`).pipe(
      retry(1),
      catchError(this.handleError)
    );
  }


  listarReportesReserva(id) {
    return this.get(
      `${this.path}/listarReportesByReserva?idReserva=${id}`).pipe(
      retry(1),
      catchError(this.handleError)
    );
  }

  listarReportesAlumno(id) {
    return this.get(
      `${this.path}/listarReportesByALumno?idAlumno=${id}`).pipe(
      retry(1),
      catchError(this.handleError)
    );
  }

  descargarReporte(id) {
    return `${this.urlDownload}/descargarReportesPdf?idReporte=${id}`;
  }

  cargarReporte(reporte) {
    return this.post(`${this.path}/cargarReporte`, reporte).pipe(
      retry(1),
      catchError(this.handleError)
    );
  }
}
