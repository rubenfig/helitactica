import { Injectable } from '@angular/core';
import {ApiService} from './api.service';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class CursosService extends ApiService {
  path = 'cursos';
  constructor(public http: HttpClient) {
    super(http);
  }
}
