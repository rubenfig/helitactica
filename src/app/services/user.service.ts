import { Injectable } from '@angular/core';
import {catchError, retry, share} from 'rxjs/operators';
import {ApiService} from './api.service';
import {KeychainTouchId} from '@ionic-native/keychain-touch-id/ngx';
import {throwError} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  // tslint:disable-next-line:variable-name
  _user: any;

  constructor(public api: ApiService,
              private touch: KeychainTouchId) { }

  /**
   * Send a POST request to our login endpoint with the data
   * the user entered on the form.
   */
  test() {


    const seq = this.api.get('test').pipe(share());

    seq.subscribe((res: any) => {
      // If the API returned a successful response, mark the user as logged in
      console.log(res);
      if (res.estado === 0) {
      }
    }, (err) => {
      return err;
    });

    return seq;
  }


  /**
   * Send a POST request to our login endpoint with the data
   * the user entered on the form.
   */
  login(accountInfo: any) {
    console.log(accountInfo);
    const seq = this.api.post('sesion', accountInfo).pipe(share());
    seq.subscribe((res: any) => {
      console.log(res);
      // If the API returned a successful response, mark the user as logged in
      if (res.status === 0 && !res.primerPassword) {
        res.pass = accountInfo.pass;
        this._loggedIn(res);
      }
    }, (err) => {
      return err;
    });

    return seq;
  }

  habilitarUsuario(data: any) {
    return this.api.post('usuarios/habilitarUsuario', data);
  }

  /**
   * Método para resetear contraseña del usuario
   */
  resetearPassword(data: any) {

    return this.api.post('usuarios/resetearPass', data);
  }

  /**
   * Método para cambiar contraseña del usuario
   */
  cambiarPassword(data: any) {

    return this.api.post('usuarios/resetearPassUsuario', data);
  }
  /**
   * Método para obtener el contacto de la escuela
   */
  obtenerContacto() {
    return this.api.get('util/contacto');
  }

  /**
   * Método para obtener los usuarios
   */
  listarUsuarios(usuario) {
    const filtro = usuario && usuario.length ? `/?usuario=${usuario}` : '';
    return this.api.get('usuarios/listar/0' + filtro).pipe(
      retry(1),
      catchError(this.handleError)
    );
  }

  crearUsuario(usuario) {
    return this.api.post(`usuarios/addUsuario`, usuario).pipe(
      retry(1),
      catchError(this.handleError)
    );
  }

  modificarUsuario(usuario) {
    return this.api.post(`usuarios/modificarUsuario`, usuario).pipe(
      retry(1),
      catchError(this.handleError)
    );
  }

  /**
   * Método para obtener los usuarios
   */
  listarPerfiles() {
    return this.api.get('perfiles/listar/0').pipe(
      retry(1),
      catchError(this.handleError)
    );
  }

  /**
   * Log the user out, which forgets the session
   */
  logout() {
    const seq = this.api.get('sesion/cerrar', null).pipe(share());
    seq.subscribe((res: any) => {
      if (res.estado === 0) {
        this._user = null;
        localStorage.setItem('user', null);
        this.touch.delete('HELIAPP');
      }
    }, (err) => {
      return err;
    });
    return seq;
  }

  /**
   * Process a login/signup response to store user data
   */
  _loggedIn(data) {
    this._user = data;
    console.log(data);
    localStorage.setItem('user', JSON.stringify(data));
  }

  getUser() {
    return JSON.parse(localStorage.getItem('user'));
  }

  setRecordado(recordado) {
    localStorage.setItem('recordado', recordado.toString());
  }

  isRecordado() {
    return localStorage.getItem('recordado') === 'true';
  }

  handleError(error) {
    console.log(error);
    return throwError({error: true, message: 'Ocurrió un error'});
  }
}
