import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {ApiService} from './api.service';
import {catchError, retry} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ReservaService extends ApiService {
  path = 'reserva';
  constructor(public http: HttpClient) {
    super(http);
  }

  listarReservasAvion(avion, desde, hasta) {
    return this.get(`${this.path}/listar/aeronave/${avion}/?fechaInicio=${desde}&fechaFin=${hasta}`).pipe(
        retry(1),
        catchError(this.handleError)
    );
  }

  listarReservasAlumno(alumno, desde, hasta) {
    return this.get(`${this.path}/listar/alumno/${alumno}/?fechaInicio=${desde}&fechaFin=${hasta}`).pipe(
      retry(1),
      catchError(this.handleError)
    );
  }

  listarReservasInstructor(usuario, desde, hasta) {
    return this.get(`instructores/listarReservasInstructorByUser?idUsuario=${usuario}&fechaInicio=${desde}&fechaFin=${hasta}`).pipe(
      retry(1),
      catchError(this.handleError)
    );
  }

  listarTodasReservasAlumno(alumno) {
    return this.get(`${this.path}/listar/alumno/${alumno}`).pipe(
      retry(1),
      catchError(this.handleError)
    );
  }

  obtenerReserva(reserva) {
    return this.get(`${this.path}/listar/reserva/${reserva}`).pipe(
      retry(1),
      catchError(this.handleError)
    );
  }

  listarReservas(desde, hasta) {
    return this.get(`${this.path}/listar/reserva/0/?fechaInicio=${desde}&fechaFin=${hasta}`).pipe(
      retry(1),
      catchError(this.handleError)
    );
  }


  listarReservasPendientes() {
    return this.get(`${this.path}/listar/pendientes`).pipe(
      retry(1),
      catchError(this.handleError)
    );
  }

  crearReserva(reserva) {
    return this.post(`${this.path}/reservar`, reserva).pipe(
       retry(1),
       catchError(this.handleError)
    );
  }

  modificarEstado(reserva) {
    return this.post(`${this.path}/aprobar`, reserva).pipe(
      retry(1),
      catchError(this.handleError)
    );
  }

  calificarReserva(calificacion) {
    return this.post(`calificacion/calificar`, calificacion).pipe(
      retry(1),
      catchError(this.handleError)
    );
  }

  listarRechazo() {
    return this.get(`tiporechazo/listar`).pipe(
      retry(1),
      catchError(this.handleError)
    );
  }

  listarPosiciones(idReserva) {
    return this.get(`spiderTracks/listarPosiciones?idReserva=` + idReserva).pipe(
      retry(1),
      catchError(this.handleError)
    );
  }
 }
