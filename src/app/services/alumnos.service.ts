import { Injectable } from '@angular/core';
import {ApiService} from './api.service';
import {catchError, retry} from 'rxjs/operators';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class AlumnosService extends ApiService {
  path = 'alumnos';
  constructor(public http: HttpClient) {
    super(http);
  }


  agregarSaldo(idAlumno, saldo) {
    const params = `?idAlumno=${idAlumno}&saldo=${saldo}`;
    return this.get(`${this.path}/cargarsaldo` + params).pipe(
      retry(1),
      catchError(this.handleError)
    );
  }

  modificarAlumno(usuario) {
    return this.post(`${this.path}/modificarAlumno`, usuario).pipe(
      retry(1),
      catchError(this.handleError)
    );
  }

}
