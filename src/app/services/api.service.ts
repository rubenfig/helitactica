import { Injectable } from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {throwError} from 'rxjs';
import {catchError, retry} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  url = 'http://www.servivuelospy.ml:23080/ServiVuelos-web/webresources';
  urlDownload = 'http://www.servivuelospy.ml:23080/ServiVuelos-web';
  // url = 'http://ec2-18-190-26-74.us-east-2.compute.amazonaws.com:23080/ServiVuelos-web/webresources';
  // urlDownload = 'http://ec2-18-190-26-74.us-east-2.compute.amazonaws.com:23080/ServiVuelos-web';
  path = '';
  constructor(public http: HttpClient) {
  }

  get(endpoint: string, params?: any, reqOpts?: any) {
    if (!reqOpts) {
      reqOpts = {
        params: new HttpParams()
      };
    }

    // Support easy query params for GET requests
    if (params) {
      reqOpts.params = new HttpParams();
      // tslint:disable-next-line:forin
      for (const k in params) {
        reqOpts.params = reqOpts.params.set(k, params[k]);
      }
    }
    console.log(this.url + '/' + endpoint);
    return this.http.get(this.url + '/' + endpoint, reqOpts);
  }

  post(endpoint: string, body: any, reqOpts?: any) {
    console.log(endpoint);
    return this.http.post(this.url + '/' + endpoint, body, reqOpts);
  }

  put(endpoint: string, body: any, reqOpts?: any) {
    return this.http.put(this.url + '/' + endpoint, body, reqOpts);
  }

  delete(endpoint: string, reqOpts?: any) {
    return this.http.delete(this.url + '/' + endpoint, reqOpts);
  }

  patch(endpoint: string, body: any, reqOpts?: any) {
    return this.http.patch(this.url + '/' + endpoint, body, reqOpts);
  }

  handleError(error) {
    console.log(error);
    return throwError({error: true, message: 'Ocurrió un error'});
  }


  listar() {
    return this.get(`${this.path}/listar`).pipe(
        retry(1),
        catchError(this.handleError)
    );
  }

  obtener(id) {
    return this.get(`${this.path}/listar/${id}`).pipe(
      retry(1),
      catchError(this.handleError)
    );
  }
}
