import { Injectable } from '@angular/core';
import {ApiService} from './api.service';
import {HttpClient} from '@angular/common/http';
import {catchError, retry} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class InstructoresService extends ApiService {
  path = 'instructores';
  constructor(public http: HttpClient) {
    super(http);
  }

  cargarHorario(horario, limpiar) {
    return this.post(limpiar ? `${this.path}/limpiarDiasInstructor` : `${this.path}/cargarHorario`, horario).pipe(
      retry(1),
      catchError(this.handleError)
    );
  }
}
