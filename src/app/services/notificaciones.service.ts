import { Injectable } from '@angular/core';
import {ApiService} from './api.service';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {catchError, retry} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class NotificacionesService extends ApiService {
  path = 'notificaciones';
  constructor(public http: HttpClient) {
    super(http);
  }

  listarNotificaciones(usuario) {
    return this.get(`${this.path}/listar/${usuario}`).pipe(
      retry(1),
      catchError(this.handleError)
    );
  }
}
