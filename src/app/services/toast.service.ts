import { Injectable } from '@angular/core';
import {ToastController} from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class ToastService {
  constructor(public toastCtrl: ToastController) { }

  showDefaultToast = async (message) => {
    const toast = await this.toastCtrl.create({
      message,
      position: 'bottom',
      showCloseButton: true,
      duration: 15000,
      closeButtonText: 'Cerrar'
    });

    toast.present();
  }

}
