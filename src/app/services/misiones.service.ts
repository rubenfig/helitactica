import { Injectable } from '@angular/core';
import {ApiService} from './api.service';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class MisionesService extends ApiService {
  path = 'misiones';
  constructor(public http: HttpClient) {
    super(http);
  }

}
