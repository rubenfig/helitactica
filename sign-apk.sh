ionic cordova build android --prod --release
cd platforms/android/app/build/outputs/apk/release
rm heli.apk
jarsigner -verbose -sigalg SHA1withRSA -digestalg SHA1 -keystore heli.keystore app-release-unsigned.apk heli
$ANDROID_HOME/build-tools/28.0.3/zipalign -v 4 app-release-unsigned.apk heli.apk
